/* $Id$ */

/* generate_defs_gnomevfs.h
 *
 * Copyright (C) 2001 The Free Software Foundation
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include "glibmm_generate_extra_defs/generate_extra_defs.h"
#include <libgnomevfs/gnome-vfs.h>
//#include <libgnomevfs/gnome-vfs-metadata.h>
#include <libgnomevfs/gnome-vfs-mime-monitor.h>
#include <libgnomevfs/gnome-vfs-volume-monitor.h>

int main (int argc, char *argv[])
{
    gnome_vfs_init();
    
    std::cout // <<  get_defs(GNOME_TYPE_VFS_METADATA)
                 << get_defs(GNOME_VFS_MIME_MONITOR_TYPE)
                 << get_defs(GNOME_VFS_TYPE_VOLUME_MONITOR)
                 << get_defs(GNOME_VFS_TYPE_DRIVE)
                 << get_defs(GNOME_VFS_TYPE_VOLUME);
    return 0;
}
