_CONVERSION(`GnomeVFSURI*',`Glib::RefPtr<Uri>',`Glib::wrap($3)')
_CONVERSION(`GnomeVFSURI*',`Glib::RefPtr<const Uri>',`Glib::wrap($3)')
_CONVERSION(`const Glib::RefPtr<Uri>&', `GnomeVFSURI*', `Glib::unwrap($3)')
_CONVERSION(`const Glib::RefPtr<const Uri>&', `GnomeVFSURI*', `Glib::unwrap(const_cast<$2>($3))')
_CONVERSION(`const Glib::RefPtr<const Uri>&', `const GnomeVFSURI*', `Glib::unwrap($3)')

_CONVERSION(`GnomeVFSFileInfo*', `Glib::RefPtr<FileInfo>', `Glib::wrap($3)')
_CONVERSION(`const Glib::RefPtr<const FileInfo>&', `const GnomeVFSFileInfo*', `Glib::unwrap($3)')

_CONVERSION(`GnomeVFSDrive*',`const Glib::RefPtr<Drive>',`Glib::wrap($3)')
_CONVERSION(`GnomeVFSDrive*',`const Glib::RefPtr<Drive>&',`Glib::wrap($3)')
_CONVERSION(`GnomeVFSDrive*',`Glib::RefPtr<Drive>',`Glib::wrap($3)')
_CONVERSION(`GnomeVFSDrive*',`Glib::RefPtr<const Drive>',`Glib::wrap($3)')
_CONVERSION(`const Glib::RefPtr<Drive>',`GnomeVFSDrive*',`Glib::unwrap($3)')
_CONVERSION(`const Glib::RefPtr<Drive>&',`GnomeVFSDrive*',`Glib::unwrap($3)')
_CONVERSION(`const Glib::RefPtr<const Drive>&',`GnomeVFSDrive*',`const_cast<$2>(Glib::unwrap($3))')

_CONVERSION(`GnomeVFSVolume*',`const Glib::RefPtr<Volume>',`Glib::wrap($3)')
_CONVERSION(`GnomeVFSVolume*',`const Glib::RefPtr<const Volume>',`Glib::wrap($3)')
_CONVERSION(`GnomeVFSVolume*',`const Glib::RefPtr<Volume>&',`Glib::wrap($3)')
_CONVERSION(`GnomeVFSVolume*',`Glib::RefPtr<Volume>',`Glib::wrap($3)')
_CONVERSION(`GnomeVFSVolume*',`Glib::RefPtr<const Volume>',`Glib::wrap($3)')
_CONVERSION(`const Glib::RefPtr<Volume>',`GnomeVFSVolume*',`Glib::unwrap($3)')
_CONVERSION(`const Glib::RefPtr<Volume>&',`GnomeVFSVolume*',`Glib::unwrap($3)')
_CONVERSION(`const Glib::RefPtr<const Volume>&',`GnomeVFSVolume*',`const_cast<$2>(Glib::unwrap($3))')

_CONVERSION(`GnomeVFSVolumeMonitor*',`Glib::RefPtr<VolumeMonitor>',`Glib::wrap($3)')

_CONVERSION(`GList*',`Glib::ListHandle< Glib::RefPtr<Volume> >',__FL2H_SHALLOW)
_CONVERSION(`GList*',`Glib::ListHandle< Glib::RefPtr<const Volume> >',__FL2H_SHALLOW)
_CONVERSION(`GList*',`Glib::ListHandle< Glib::RefPtr<Drive> >',__FL2H_SHALLOW)
_CONVERSION(`GList*',`Glib::ListHandle< Glib::RefPtr<const Drive> >',__FL2H_SHALLOW)

_CONVERSION(`char*',`std::string',__GCHARP_TO_STDSTRING)
_CONVERSION(`Glib::ustring',`char*',`const_cast<$2>($3.c_str())')

_CONVERSION(`Glib::ListHandle<Glib::ustring>',`GList*',`$3.data()')
_CONVERSION(`GList*',`Glib::ListHandle<Glib::ustring>',`$2($3, Glib::OWNERSHIP_SHALLOW)')

_CONVERSION(`GnomeVFSMimeApplication*', `MimeApplication', `Glib::wrap($3)')


_CONVERSION(`GnomeVFSToplevelURI*',`const GnomeVFSToplevelURI*',`const_cast<const GnomeVFSToplevelURI*>($3)')

dnl Enumeration and various typedef conversions:
dnl --------------------------------------------

# We use C casts instead of static_cast<> because gcc 2.95.4 has a problem with them:
# We should patch libgnomevfs to register the gtypes anyway. Murray.
_CONVERSION(`Result', `GnomeVFSResult', `($2)($3)')
_CONVERSION(`GnomeVFSResult', `Result', `($2)($3)')

_CONVERSION(`URIHideOptions', `GnomeVFSURIHideOptions', `($2)($3)')
_CONVERSION(`GnomeVFSURIHideOptions', `URIHideOptions', `($2)($3)')

_CONVERSION(`FileInfoOptions', `GnomeVFSFileInfoOptions', `($2)($3)')
_CONVERSION(`GnomeVFSFileInfoOptions', `FileInfoOptions', `($2)($3)')

_CONVERSION(`DirectoryVisitOptions', `GnomeVFSDirectoryVisitOptions', `($2)($3)')
_CONVERSION(`GnomeVFSDirectoryVisitOptions', `DirectoryVisitOptions', `($2)($3)')

_CONVERSION(`OpenMode', `GnomeVFSOpenMode', `($2)($3)')
_CONVERSION(`GnomeVFSOpenMode', `OpenMode', `($2)($3)')

_CONVERSION(`FileSize', `GnomeVFSFileSize', `($2)($3)')
_CONVERSION(`GnomeVFSFileSize', `FileSize', `($2)($3)')

_CONVERSION(`FileFlags', `GnomeVFSFileFlags', `($2)($3)')
_CONVERSION(`GnomeVFSFileFlags', `FileFlags', `($2)($3)')

_CONVERSION(`FileType', `GnomeVFSFileType', `($2)($3)')
_CONVERSION(`GnomeVFSFileType', `FileType', `($2)($3)')

_CONVERSION(`FilePermissions', `GnomeVFSFilePermissions', `($2)($3)')
_CONVERSION(`GnomeVFSFilePermissions', `FilePermissions', `($2)($3)')

_CONVERSION(`SetFileInfoMask', `GnomeVFSSetFileInfoMask', `($2)($3)')
_CONVERSION(`GnomeVFSSetFileInfoMask', `SetFileInfoMask', `($2)($3)')

_CONVERSION(`FileOffset', `GnomeVFSFileOffset', `($2)($3)')
_CONVERSION(`GnomeVFSFileOffset', `FileOffset', `($2)($3)')

_CONVERSION(`MimeApplicationArgumentType', `GnomeVFSMimeApplicationArgumentType', `($2)($3)')
_CONVERSION(`GnomeVFSMimeApplicationArgumentType', `MimeApplicationArgumentType', `($2)($3)')

_CONVERSION(`DeviceType',`GnomeVFSDeviceType',`($2)($3)')
_CONVERSION(`GnomeVFSDeviceType',`DeviceType',`($2)($3)')

_CONVERSION(`VolumeType',`GnomeVFSVolumeType',`($2)($3)')
_CONVERSION(`GnomeVFSVolumeType',`VolumeType',`($2)($3)')

_CONV_ENUM(GnomeVFS,MakeURIDirs)

_CONVERSION(`GnomeVFSMIMEMonitor*',`Glib::RefPtr<MimeMonitor>',`Glib::wrap($3)')
