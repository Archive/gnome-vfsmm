// -*- c++ -*-
#ifndef _LIBGNOMEVFSMM_UTILS_H
#define _LIBGNOMEVFSMM_UTILS_H
/* $Id$ */

/* utils.h
 *
 * Copyright 2004      gnome-vfsmm development team.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <libgnomevfsmm/types.h>
#include <libgnomevfsmm/exception.h>
#include <glibmm/ustring.h>


namespace Gnome
{

namespace Vfs
{

/** Formats the file size passed in @a sizein a way that is easy for
 * the user to read. Gives the size in bytes, kilobytes, megabytes or
 * gigabytes, choosing whatever is appropriate.
 *
 * @param size
 * @result a newly allocated string with the size ready to be shown.
 **/
Glib::ustring format_file_size_for_display(FileSize  size);

/** Escapes @unescaped_string, replacing any and all special characters with equivalent escape sequences.
 *
 * @param unscaped_string String to be escaped
 * @result A string equivalent to @a unescaped_string but with all special characters escaped
 */
Glib::ustring escape_string(const Glib::ustring& unescaped_string);

/** Escapes @path, replacing only special characters that would not
 * be found in paths (so '/', '&', '=', and '?' will not be escaped by this function).
 *
 * @param path String to be escaped
 * @result A string equivalent to @a path but with non-path characters escaped
 */
Glib::ustring escape_path_string(const Glib::ustring& path);

/** Escapes @a path, replacing only special characters that would not
 * be found in paths or host name (so '/', '&', '=', ':', '@' and '?' will not be escaped by this function).
 *
 * @param path String to be escaped
 * Return value A string equivalent to @a path but with non-path/host characters escaped
 */
Glib::ustring escape_host_and_path_string(const Glib::ustring& path);

/** Escapes only '/' and '%' characters in @string, replacing
 * them with their escape sequence equivalents.
 *
 * @param unescaped_string String to be escaped
 * @result A string equivalent to @a unescaped_string, but with no unescaped '/' or '%' characters
 */
Glib::ustring escape_slashes(const Glib::ustring& unescaped_string);

/** Decodes escaped characters (i.e. PERCENTxx sequences) in @a escaped_string.
 * Characters are encoded in PERCENTxy form, where xy is the ASCII hex code
 * for character 16x+y.
 *
 * @param escaped_string An escaped URI, path, or other string
 * @param illegal_characters A string containing a sequence of characters considered "illegal", '\0' is automatically in this list.
 * @result A newly allocated string with the unescaped equivalents,
 * or an empty string if @a escaped_string contained one of the characters
 * in @a illegal_characters.
 */
Glib::ustring unescape_string(const Glib::ustring& escaped_string, const Glib::ustring& illegal_characters = Glib::ustring());

/** Similar to unescape_string(), but it returns something
 * semi-intelligable to a user even upon receiving traumatic input
 * such as %00 or URIs in bad form.
 *
 * WARNING: You should never use this function on a whole URI!  It
 * unescapes reserved characters, and can result in a mangled URI
 * that can not be re-entered.  For example, it unescapes "#" "&" and "?",
 * which have special meanings in URI strings.
 *
 * @see unescape_string().
 *
 * @param escaped_string The string encoded with escaped sequences.
 * @result A string with all characters replacing their escaped hex values
 */
Glib::ustring unescape_string_for_display(const Glib::ustring& escaped_string);

/**
 * gnome_vfs_make_path_name_canonical:
 * @path: a file path, relative or absolute
 *
 * Calls _gnome_vfs_canonicalize_pathname, allocating storage for the
 * result and providing for a cleaner memory management.
 *
 * @result a canonical version of @path
 **/
Glib::ustring make_uri_canonical(const Glib::ustring& uri);

Glib::ustring make_path_name_canonical(const Glib::ustring& path);

/** If @a path starts with a ~, representing the user's home
 * directory, expand it to the actual path location.
 * 
 * @param path a local file path which may start with a '~'
 * @result a newly allocated string with the initial
 * tilde (if there was one) converted to an actual path
 **/
Glib::ustring expand_initial_tilde(const Glib::ustring& path);


/** Create a local path for a file:/// URI. Do not use with URIs
 * of other methods.
 *
 * @param uri  URI to convert to a local path
 * @result a string containing the local path, or an empty string if the uri isn't a file: URI
 * without a fragment identifier (or chained URI).
 **/
Glib::ustring get_local_path_from_uri(const Glib::ustring& uri);

/** Returns a file:/// URI for the local path @local_full_path.
 *
 * @param local_full_path A full local filesystem path (i.e. not relative)
 * @result A string containing the URI corresponding to @a local_full_path.
 */
Glib::ustring get_uri_from_local_path(const Glib::ustring& local_full_path);

/** Checks if @a command_string starts with the full path of an executable file
 * or an executable in $PATH.
 *
 * @param command_string:
 * @result true if command_string started with an executable file, false otherwise.
 */
bool is_executable_command_string(const Glib::ustring& command_string);

Glib::ustring icon_path_from_filename(const Glib::ustring& filename);

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void url_show (const Glib::ustring& url) throw(exception);
#else
void url_show (const Glib::ustring& url, std::auto_ptr<Gnome::Vfs::exception>& error);
#endif //GLIBMM_EXCEPTIONS_ENABLED

//TODO: Where does this envp come from? It seems to be an input parameter.
//void url_show (const Glib::ustring& url, char** envp) throw(exception);

} //namespace Vfs
} //namespace Gnome

#endif //_LIBGNOMEVFSMM_UTILS_H

