// -*- c++ -*-
/* $Id$ */

/* utils.cc
 *
 * Copyright 2004      gnome-vfsmm development team.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <libgnomevfsmm/utils.h>
#include <libgnomevfs/gnome-vfs-utils.h>
#include <libgnomevfsmm/private.h>
#include <glibmm/utility.h> //For Glib::convert_const_gchar_ptr_to_ustring()

namespace Gnome
{

namespace Vfs
{

Glib::ustring format_file_size_for_display(FileSize size)
{
  return Glib::convert_const_gchar_ptr_to_ustring( gnome_vfs_format_file_size_for_display(size) );
}

Glib::ustring escape_string(const Glib::ustring& unescaped_string)
{
  return Glib::convert_const_gchar_ptr_to_ustring( gnome_vfs_escape_string(unescaped_string.c_str()) );
}

Glib::ustring escape_path_string(const Glib::ustring& path)
{
  return Glib::convert_const_gchar_ptr_to_ustring( gnome_vfs_escape_path_string(path.c_str()) );
}

Glib::ustring escape_host_and_path_string(const Glib::ustring& path)
{
  return Glib::convert_const_gchar_ptr_to_ustring( gnome_vfs_escape_host_and_path_string(path.c_str()) );
}

Glib::ustring escape_slashes(const Glib::ustring& unescaped_string)
{
  return Glib::convert_const_gchar_ptr_to_ustring( gnome_vfs_escape_slashes(unescaped_string.c_str()) );
}

Glib::ustring unescape_string(const Glib::ustring& escaped_string, const Glib::ustring& illegal_characters)
{
  return Glib::convert_const_gchar_ptr_to_ustring( gnome_vfs_unescape_string(escaped_string.c_str(), (illegal_characters.empty() ? 0 : illegal_characters.c_str())) );
}

Glib::ustring unescape_string_for_display(const Glib::ustring& escaped_string)
{
  return Glib::convert_const_gchar_ptr_to_ustring( gnome_vfs_unescape_string_for_display(escaped_string.c_str()) );
}

Glib::ustring make_uri_canonical(const Glib::ustring& uri)
{
  return Glib::convert_const_gchar_ptr_to_ustring( gnome_vfs_unescape_string_for_display(uri.c_str()) );
}

Glib::ustring make_path_name_canonical(const Glib::ustring& path)
{
  return Glib::convert_const_gchar_ptr_to_ustring( gnome_vfs_unescape_string_for_display(path.c_str()) );
}

Glib::ustring expand_initial_tilde(const Glib::ustring& path)
{
  return Glib::convert_const_gchar_ptr_to_ustring( gnome_vfs_unescape_string_for_display(path.c_str()) );
}

Glib::ustring get_local_path_from_uri (const Glib::ustring& uri)
{
  return Glib::convert_const_gchar_ptr_to_ustring( gnome_vfs_get_local_path_from_uri(uri.c_str()) );
}

Glib::ustring get_uri_from_local_path(const Glib::ustring& local_full_path)
{
  return Glib::convert_const_gchar_ptr_to_ustring( gnome_vfs_get_uri_from_local_path(local_full_path.c_str()) );
}

bool is_executable_command_string(const Glib::ustring& command_string)
{
  return gnome_vfs_is_executable_command_string(command_string.c_str());
}

Glib::ustring icon_path_from_filename (const Glib::ustring& filename)
{
  return Glib::convert_const_gchar_ptr_to_ustring( gnome_vfs_icon_path_from_filename(filename.c_str()) );
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void url_show (const Glib::ustring& url) throw(exception)
#else
void url_show (const Glib::ustring& url, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GnomeVFSResult result = gnome_vfs_url_show(url.c_str());
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}

/*
void url_show (const Glib::ustring& url, char** envp) throw(exception)
{
  char* pchEnv = 0;
  GnomeVFSResult result = gnome_vfs_url_show_with_env(url.c_str(), envp);
  handle_result(result);
}
*/

} //namespace Vfs
} //namespace Gnome



