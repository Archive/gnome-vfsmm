/* Copyright 2003, 2006 gnome-vfsmm Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <libgnomevfsmm/transfer.h>
#include <libgnomevfsmm/private.h>
#include <libgnomevfs/gnome-vfs-xfer.h>
#include <iostream> //Just for debugging.


namespace Gnome
{

namespace Vfs
{

namespace
{

//SignalProxy_Progress:

//This Signal Proxy allows the C++ coder to specify a sigc::slot instead of a static function.
class SignalProxy_Progress
{
public:
  typedef Gnome::Vfs::Transfer::SlotProgress SlotType;

  SignalProxy_Progress(const SlotType& slot);
  ~SignalProxy_Progress();

  static gint c_callback(GnomeVFSXferProgressInfo *info, gpointer data);

protected:
  SlotType slot_;
};

SignalProxy_Progress::SignalProxy_Progress(const SlotType& slot)
:
  slot_ (slot)
{}

SignalProxy_Progress::~SignalProxy_Progress()
{}

gint SignalProxy_Progress::c_callback(GnomeVFSXferProgressInfo* info, gpointer data)
{
  SignalProxy_Progress *const self = static_cast<SignalProxy_Progress*>(data);
  Gnome::Vfs::Transfer::ProgressInfo progInfo(info);
  gint result = FALSE;
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  try
  {
  #endif //GLIBMM_EXCEPTIONS_ENABLED
    result = (self->slot_)(progInfo);
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  }
  catch(...)
  {
    Glib::exception_handlers_invoke();
  }
  #endif //GLIBMM_EXCEPTIONS_ENABLED

  return result;
}

} //anonymous namespace

namespace Transfer
{

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void transfer_list(const Glib::StringArrayHandle& source_uri_list, const Glib::StringArrayHandle& target_uri_list,
      TransferOptions options,
      ErrorMode error_mode,
      OverwriteMode overwrite_mode,
      const SlotProgress& slot)
#else
void transfer_list(const Glib::StringArrayHandle& source_uri_list, const Glib::StringArrayHandle& target_uri_list,
      TransferOptions options,
      ErrorMode error_mode,
      OverwriteMode overwrite_mode,
      const SlotProgress& slot,
      std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  typedef std::list< Glib::RefPtr<const Uri> > uri_list;
  uri_list sources, targets;

  //Build lists of RefPtr<Uri>s from the strings:
  Glib::StringArrayHandle::const_iterator iter_target = target_uri_list.begin();
  for(Glib::StringArrayHandle::const_iterator iter = source_uri_list.begin(); iter != source_uri_list.end(); ++iter)
  {
    if(iter_target != target_uri_list.end())
    {
      sources.push_back( Uri::create(*iter) );
      targets.push_back( Uri::create(*iter_target) );
      iter_target++;
    }
  }

  ListHandleUris temp = sources;
  ListHandleUris temp2 = targets;
            
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  transfer_list_uris(sources, targets, options, error_mode, overwrite_mode, slot);
  #else
  transfer_list_uris(sources, targets, options, error_mode, overwrite_mode, slot, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void transfer_list_uris(const ListHandleUris& source_uri_list, const ListHandleUris& target_uri_list,
      TransferOptions options, 
      ErrorMode error_mode,
      OverwriteMode overwrite_mode,
      const SlotProgress& slot)
#else
void transfer_list_uris(const ListHandleUris& source_uri_list, const ListHandleUris& target_uri_list,
      TransferOptions options, 
      ErrorMode error_mode,
      OverwriteMode overwrite_mode,
      const SlotProgress& slot,
      std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  SignalProxy_Progress proxy(slot);
  //TODO: Check the memory management with data() here.
  GnomeVFSResult result = gnome_vfs_xfer_uri_list(source_uri_list.data(), target_uri_list.data(),
    static_cast<GnomeVFSXferOptions>(options),
    static_cast<GnomeVFSXferErrorMode>(error_mode),
    static_cast<GnomeVFSXferOverwriteMode>(overwrite_mode),
    &SignalProxy_Progress::c_callback, &proxy);

  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void transfer(const Glib::ustring& source_uri, const Glib::ustring& target_uri,
      TransferOptions options,
      ErrorMode error_mode,
      OverwriteMode overwrite_mode,
      const SlotProgress& slot)
#else
void transfer(const Glib::ustring& source_uri, const Glib::ustring& target_uri,
      TransferOptions options,
      ErrorMode error_mode,
      OverwriteMode overwrite_mode,
      const SlotProgress& slot,
      std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  return transfer( Uri::create(source_uri), Uri::create(target_uri), options, error_mode, overwrite_mode, slot);
  #else
  return transfer( Uri::create(source_uri), Uri::create(target_uri), options, error_mode, overwrite_mode, slot, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void transfer(const Glib::RefPtr<const Uri>& source_uri, const Glib::RefPtr<const Uri>& target_uri,
      TransferOptions options, 
      ErrorMode error_mode,
      OverwriteMode overwrite_mode,
      const SlotProgress& slot)
#else
void transfer(const Glib::RefPtr<const Uri>& source_uri, const Glib::RefPtr<const Uri>& target_uri,
      TransferOptions options, 
      ErrorMode error_mode,
      OverwriteMode overwrite_mode,
      const SlotProgress& slot,
      std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  SignalProxy_Progress proxy(slot);
  GnomeVFSResult result = gnome_vfs_xfer_uri(source_uri->gobj(), target_uri->gobj(),
    static_cast<GnomeVFSXferOptions>(options),
    static_cast<GnomeVFSXferErrorMode>(error_mode),
    static_cast<GnomeVFSXferOverwriteMode>(overwrite_mode),
    &SignalProxy_Progress::c_callback, &proxy);
    
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void remove(const Glib::ustring& source_uri,
      TransferOptions options,
      ErrorMode error_mode,
      const SlotProgress& slot)
#else
void remove(const Glib::ustring& source_uri,
      TransferOptions options,
      ErrorMode error_mode,
      const SlotProgress& slot,
      std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  remove( Uri::create(source_uri), options, error_mode, slot);
  #else
  remove( Uri::create(source_uri), options, error_mode, slot, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void remove(const Glib::RefPtr<const Uri>& source_uri,
      TransferOptions options,
      ErrorMode error_mode,
      const SlotProgress& slot)
#else
void remove(const Glib::RefPtr<const Uri>& source_uri,
      TransferOptions options,
      ErrorMode error_mode,
      const SlotProgress& slot,
      std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  std::list< Glib::RefPtr<const Uri> > listUris;
  listUris.push_back(source_uri);
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  remove_list_uris(listUris, options, error_mode, slot);
  #else
  remove_list_uris(listUris, options, error_mode, slot, error);
  #endif
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void remove_list(const Glib::StringArrayHandle& source_uri_list,
      TransferOptions options,
      ErrorMode error_mode,
      const SlotProgress& slot)
#else
void remove_list(const Glib::StringArrayHandle& source_uri_list,
      TransferOptions options,
      ErrorMode error_mode,
      const SlotProgress& slot,
      std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  std::list< Glib::RefPtr<const Uri> > list_uris;
  
  //Build lists of RefPtr<Uri>s from the strings:
  Glib::StringArrayHandle::const_iterator it(source_uri_list.begin());
  Glib::StringArrayHandle::const_iterator end(source_uri_list.end());
  for( ; it != end; ++it)
  {
    list_uris.push_back(Uri::create(*it));
  }
  
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  remove_list_uris(list_uris, options, error_mode, slot);
  #else
  remove_list_uris(list_uris, options, error_mode, slot, error);
  #endif
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void remove_list_uris(const ListHandleUris& source_uri_list,
      TransferOptions options,
      ErrorMode error_mode,
      const SlotProgress& slot)
#else
void remove_list_uris(const ListHandleUris& source_uri_list,
      TransferOptions options,
      ErrorMode error_mode,
      const SlotProgress& slot,
      std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  SignalProxy_Progress proxy(slot);
  GnomeVFSResult result = gnome_vfs_xfer_delete_list(source_uri_list.data(),
    static_cast<GnomeVFSXferErrorMode>(error_mode),
    static_cast<GnomeVFSXferOptions>(options),
    &SignalProxy_Progress::c_callback, &proxy);

  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}

} // namespace Transfer
} // namespace Vfs
} // namespace Gnome
