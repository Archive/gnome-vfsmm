/* Copyright 2006 gnome-vfsmm Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#ifndef _LIBGNOMEVFSMM_TRANSFER_2_H
#define _LIBGNOMEVFSMM_TRANSFER_2_H

#include <glibmm.h>

#include <libgnomevfsmm/enums.h>
#include <libgnomevfsmm/exception.h>
#include <libgnomevfsmm/uri.h>
#include <libgnomevfsmm/transfer-progress-2.h>

#include <libgnomevfs/gnome-vfs-xfer.h> //For GnomeVFSXferProgressInfo

namespace Gnome
{

namespace Vfs
{

/** File transfers: conveniently copy/move/delete single or many URIs at once.
 */
namespace Transfer2
{

typedef Glib::ListHandle<Glib::ustring> ListHandleStrings;
typedef Glib::ListHandle< Glib::RefPtr<const Uri> > ListHandleUris;

/// For instance, gint on_transfer_progress(const Gnome::Vfs::Transfer::ProgressInfo& info);
// Essentially, this line, a different definition of SlotProgress,
// is the main change forcing the introduction of the Transfer2 namespace.
typedef sigc::slot<gint, const ProgressInfo&> SlotProgress;

/** This function will transfer multiple files to multiple targets, given
 * source URIs and destination URIs. There are a list of options that
 * your application can use to control how the transfer is done.
 * If you want to do asynchronous file transfers, you have to use
 * the functionality provided by the Async2 namespace instead.
 *
 * @param source_uri_list A list of source URI strings (ie file://).
 * @param target_uri_list A list of destination URI strings (ie file://).
 * @param options TransferOptions defining the desired operation and parameters.
 * @param error_mode An ErrorMode specifying how to proceed if a VFS error
 * occurs. For instance, you could set your application to return an error code
 * in case of an interuption.
 * @param overwrite_mode An OverwriteMode to define how to react if a file you
 * are copying is being overwritten.
 * @param progress_callback A callback used to monitor the progress of
 * a transfer. Common use would be to check to see if the transfer is asking
 * for permission to overwrite a file.
 * @throws exception
 */
#ifdef GLIBMM_EXCEPTIONS_ENABLED
void transfer_list(const Glib::StringArrayHandle& source_uri_list, const Glib::StringArrayHandle& target_uri_list,
      TransferOptions options,
      ErrorMode error_mode,
      OverwriteMode overwrite_mode,
      const SlotProgress& slot);
#else
void transfer_list(const Glib::StringArrayHandle& source_uri_list, const Glib::StringArrayHandle& target_uri_list,
      TransferOptions options,
      ErrorMode error_mode,
      OverwriteMode overwrite_mode,
      const SlotProgress& slot,
      std::auto_ptr<Gnome::Vfs::exception> error);
#endif //GLIBMM_EXCEPTIONS_ENABLED
      
/** This function will transfer multiple files to multiple targets, given
 * source URIs and destination URIs. There are a list of options that
 * your application can use to control how the transfer is done.
 * If you want to do asynchronous file transfers, you must use
 * the functionality provided by the Async2 namespace instead.
 *
 * @param source_uri_list A Uri list.
 * @param target_uri_list A Uri list.
 * @param options TransferOptions defining the desired operation and
 * parameters.
 * @param error_mode: An ErrorMode specifying how to proceed if a VFS error
 * occurs. For instance, you could set your application to return an error code
 * in case of an interuption.
 * @param overwrite_mode An OverwriteMode to define how to react if a file you
 * are copying is being overwritten.
 * @param progress_callback A callback used to monitor the progress of
 * a transfer. Common use would be to check to see if the transfer is asking
 * for permission to overwrite a file.
 * @throws exception
 */
#ifdef GLIBMM_EXCEPTIONS_ENABLED
void transfer_list_uris(const ListHandleUris& source_uri_list, const ListHandleUris& target_uri_list,
      TransferOptions options = XFER_DEFAULT,
      ErrorMode error_mode = XFER_ERROR_MODE_ABORT,
      OverwriteMode overwrite_mode = XFER_OVERWRITE_MODE_ABORT,
      const SlotProgress& progress_callback = SlotProgress());
#else
void transfer_list_uris(const ListHandleUris& source_uri_list, const ListHandleUris& target_uri_list,
      TransferOptions options,
      ErrorMode error_mode,
      OverwriteMode overwrite_mode,
      const SlotProgress& progress_callback,
      std::auto_ptr<Gnome::Vfs::exception>& error);
#endif //GLIBMM_EXCEPTIONS_ENABLED

/** This function works exactly like transfer_list(), and is a
 * convenience wrapper for only acting on one source/target URI pair.
 *
 * @see transfer_list()
 */
#ifdef GLIBMM_EXCEPTIONS_ENABLED
void transfer(const Glib::ustring& source_uri, const Glib::ustring& target_uri,
      TransferOptions options = XFER_DEFAULT,
      ErrorMode error_mode = XFER_ERROR_MODE_ABORT,
      OverwriteMode overwrite_mode = XFER_OVERWRITE_MODE_ABORT,
      const SlotProgress& progress_callback = SlotProgress());
#else
void transfer(const Glib::ustring& source_uri, const Glib::ustring& target_uri,
      TransferOptions options,
      ErrorMode error_mode,
      OverwriteMode overwrite_mode,
      const SlotProgress& progress_callback,
      std::auto_ptr<Gnome::Vfs::exception>& error);
#endif //GLIBMM_EXCEPTIONS_ENABLED

/** This function works exactly like transfer_list_uris(), and is a
 * convenience wrapper for only acting on one source/target URI pair.
 *
 * @see transfer_list()
 */
#ifdef GLIBMM_EXCEPTIONS_ENABLED
void transfer(const Glib::RefPtr<const Uri>& source_uri, const Glib::RefPtr<const Uri>& target_uri,
      TransferOptions options = XFER_DEFAULT,
      ErrorMode error_mode = XFER_ERROR_MODE_ABORT,
      OverwriteMode overwrite_mode = XFER_OVERWRITE_MODE_ABORT,
      const SlotProgress& progress_callback = SlotProgress());
#else
void transfer(const Glib::RefPtr<const Uri>& source_uri, const Glib::RefPtr<const Uri>& target_uri,
      TransferOptions options,
      ErrorMode error_mode,
      OverwriteMode overwrite_mode,
      const SlotProgress& progress_callback,
      std::auto_ptr<Gnome::Vfs::exception>& error);
#endif //GLIBMM_EXCEPTIONS_ENABLED

//TODO: Add a string-based version of gnome_vfs_xfer_delete() to gnome-vfs.

/** Unlinks items in the source_uri_list from their filesystems.
 *
 * @param source_uri_list A string list containing URIs (ie file://).
 * @param error_mode Decide how you want to deal with interruptions.
 * @param xfer_options Set whatever transfer options you need.
 * @param progress_callback Callback to check on progress of transfer.
 * @param data Data to be passed back in callbacks from the transfer engine.
 * @throws exception
 */
#ifdef GLIBMM_EXCEPTIONS_ENABLED
void remove_list(const Glib::StringArrayHandle& source_uri_list,
      TransferOptions options,
      ErrorMode error_mode,
      const SlotProgress& slot);
#else
void remove_list(const Glib::StringArrayHandle& source_uri_list,
      TransferOptions options,
      ErrorMode error_mode,
      const SlotProgress& slot,
      std::auto_ptr<Gnome::Vfs::exception>& error);
#endif //GLIBMM_EXCEPTIONS_ENABLED

/** Unlinks items in the source_uri_list from their filesystems.
 *
 * @param source_uri_list A Uri list.
 * @param error_mode Decide how you want to deal with interruptions.
 * @param xfer_options Set whatever transfer options you need.
 * @param progress_callback Callback to check on progress of transfer.
 * @param data Data to be passed back in callbacks from the transfer engine.
 * @throws exception
 */
#ifdef GLIBMM_EXCEPTIONS_ENABLED
void remove_list_uris(const ListHandleUris& source_uri_list,
      TransferOptions options = XFER_DEFAULT,
      ErrorMode error_mode = XFER_ERROR_MODE_ABORT,
      const SlotProgress& progress_callback = SlotProgress());
#else
void remove_list_uris(const ListHandleUris& source_uri_list,
      TransferOptions options,
      ErrorMode error_mode,
      const SlotProgress& progress_callbacki,
      std::auto_ptr<Gnome::Vfs::exception>& error);
#endif //GLIBMM_EXCEPTIONS_ENABLED

/** This function works exactly like remove_list(), and is a
 * convenience wrapper for only acting on one source URI.
 *
 * @see remove_list()
 */
#ifdef GLIBMM_EXCEPTIONS_ENABLED
void remove(const Glib::ustring& source_uri,
      TransferOptions options = XFER_DEFAULT,
      ErrorMode error_mode = XFER_ERROR_MODE_ABORT,
      const SlotProgress& progress_callback = SlotProgress());
#else
void remove(const Glib::ustring& source_uri,
      TransferOptions options,
      ErrorMode error_mode,
      const SlotProgress& progress_callback,
      std::auto_ptr<Gnome::Vfs::exception>& error);
#endif //GLIBMM_EXCEPTIONS_ENABLED

/** This function works exactly like remove_list_uris(), and is a
 * convenience wrapper for only acting on one source URI.
 *
 * @see remove_list_uris()
 */
#ifdef GLIBMM_EXCEPTIONS_ENABLED
void remove(const Glib::RefPtr<const Uri>& source_uri,
      TransferOptions options = XFER_DEFAULT,
      ErrorMode error_mode = XFER_ERROR_MODE_ABORT,
      const SlotProgress& progress_callback = SlotProgress());
#else
void remove(const Glib::RefPtr<const Uri>& source_uri,
      TransferOptions options,
      ErrorMode error_mode,
      const SlotProgress& progress_callback,
      std::auto_ptr<Gnome::Vfs::exception>& error);
#endif //GLIBMM_EXCEPTIONS_ENABLED

} // namespace Transfer2
} // namespace Vfs
} // namespace Gnome

#endif /* _LIBGNOMEVFSMM_TRANSFER_2_H */
