#ifndef _LIBGNOMEVFSMM_EXCEPTION_H
#define _LIBGNOMEVFSMM_EXCEPTION_H

#include <glibmm.h>

/* Copyright 2003 gnome-vfsmm Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <glibmm/exception.h>
#include <libgnomevfsmm/enums.h>
#include <libgnomevfs/gnome-vfs-ops.h>

namespace Gnome
{

namespace Vfs
{

class exception : public Glib::Exception
{
public:
  exception();
  explicit exception(Gnome::Vfs::Result result);
  virtual ~exception() throw();

  virtual Glib::ustring what() const;

protected:
  Gnome::Vfs::Result gobj_;
};

} // namespace Vfs
} // namespace Gnome

#endif /* _LIBGNOMEVFSMM_URI_H */

