/* Copyright 2003 gnome-vfsmm Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <libgnomevfsmm/mime-handlers.h>
#include <libgnomevfsmm/mime-application.h>
#include <libgnomevfsmm/private.h>

#include <libgnomevfs/gnome-vfs-mime-handlers.h>
#include <libgnomevfs/gnome-vfs-mime-info.h>

namespace Gnome
{

namespace Vfs
{

namespace Mime
{

/**********************
 * Accessor functions *
 **********************/

MimeActionType get_default_action_type(const Glib::ustring& mime_type)
{
  return static_cast<Gnome::Vfs::MimeActionType>(gnome_vfs_mime_get_default_action_type(mime_type.c_str()));
}

MimeApplication get_default_application(const Glib::ustring& mime_type)
{
  return Glib::wrap(gnome_vfs_mime_get_default_application(mime_type.c_str()));
}

GnomeVFSMimeAction* get_default_action(const Glib::ustring& mime_type)
{
  return gnome_vfs_mime_get_default_action(mime_type.c_str());
}

ListHandleApps get_short_list_applications(const Glib::ustring& mime_type)
{
  GList* pList = gnome_vfs_mime_get_short_list_applications(mime_type.c_str());
  return Glib::ListHandle<Gnome::Vfs::MimeApplication*>(pList, Glib::OWNERSHIP_SHALLOW);
}

ListHandleApps get_all_applications(const Glib::ustring& mime_type)
{
  GList* pList = gnome_vfs_mime_get_all_applications(mime_type.c_str());
  return Glib::ListHandle<Gnome::Vfs::MimeApplication*>(pList, Glib::OWNERSHIP_SHALLOW);
}

/* Removed from gnome-vfs:
ListHandleComps get_short_list_components(const Glib::ustring& mime_type)
{
  return gnome_vfs_mime_get_short_list_components(mime_type.c_str());
}
*/

/* Removed from gnome-vfs:
ListHandleComps get_all_components(const Glib::ustring& mime_type)
{
  return gnome_vfs_mime_get_all_components(mime_type.c_str());
}
*/

bool id_in_application_list(const Glib::ustring& id, const ListHandleApps& applications)
{
  return gnome_vfs_mime_id_in_application_list(id.c_str(), applications.data());
}

ListHandleStrings id_list_from_application_list(const ListHandleApps& applications)
{
  GList* pList = gnome_vfs_mime_id_list_from_application_list(applications.data());
  return ListHandleStrings(pList, Glib::OWNERSHIP_SHALLOW);
}

/* Removed from gnome-vfs:
bool id_in_component_list(const Glib::ustring& iid, ListHandleComps components)
{
  return gnome_vfs_mime_id_in_component_list(iid.c_str(), components);
}

ListHandleStrings id_list_from_component_list(ListHandleComps components)
{
  GList* pList = gnome_vfs_mime_id_list_from_component_list(components);
  return ListHandleStrings(pList, Glib::OWNERSHIP_SHALLOW);
}
*/

Glib::ustring get_icon(const Glib::ustring& mime_type)
{
  return Glib::convert_const_gchar_ptr_to_ustring(gnome_vfs_mime_get_icon(mime_type.c_str()));
}

Glib::ustring get_description(const Glib::ustring& mime_type)
{
  return Glib::convert_const_gchar_ptr_to_ustring(gnome_vfs_mime_get_description(mime_type.c_str()));
}

bool can_be_executable(const Glib::ustring& mime_type)
{
  return gnome_vfs_mime_can_be_executable(mime_type.c_str());
}

bool type_is_known(const Glib::ustring& mime_type)
{
  return gnome_vfs_mime_type_is_known(mime_type.c_str());
}

ListHandleStrings get_extensions_list(const Glib::ustring& mime_type)
{
  GList* pList = gnome_vfs_mime_get_extensions_list(mime_type.c_str());
  return Glib::ListHandle<Glib::ustring>(pList, Glib::OWNERSHIP_SHALLOW);
}

Glib::ustring get_extensions_string(const Glib::ustring& mime_type, bool pretty)
{
  return Glib::convert_const_gchar_ptr_to_ustring(pretty ? gnome_vfs_mime_get_extensions_pretty_string(mime_type.c_str()) : gnome_vfs_mime_get_extensions_string(mime_type.c_str()));
}

ListHandleStrings get_registered_types()
{
  GList* pList = gnome_vfs_get_registered_mime_types();
  return Glib::ListHandle<Glib::ustring>(pList, Glib::OWNERSHIP_SHALLOW);
}

Glib::ustring get_value(Glib::ustring& mime_type, Glib::ustring& key)
{
  return Glib::convert_const_gchar_ptr_to_ustring(gnome_vfs_mime_get_value(mime_type.c_str(), key.c_str()));
}

ListHandleStrings get_key_list(Glib::ustring& mime_type)
{
  GList* pList = gnome_vfs_mime_get_key_list(mime_type.c_str());
  return Glib::ListHandle<Glib::ustring>(pList, Glib::OWNERSHIP_SHALLOW);
}

/*********************
 * Mutator functions *
 *********************/

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void set_default_action_type(const Glib::ustring& mime_type, MimeActionType action_type) throw(exception)
#else
void set_default_action_type(const Glib::ustring& mime_type, MimeActionType action_type, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GnomeVFSResult result = gnome_vfs_mime_set_default_action_type(mime_type.c_str(), static_cast<GnomeVFSMimeActionType>(action_type));
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void set_default_application(const Glib::ustring& mime_type, const Glib::ustring& application_id) throw(exception)
#else
void set_default_application(const Glib::ustring& mime_type, const Glib::ustring& application_id, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GnomeVFSResult result = gnome_vfs_mime_set_default_application(mime_type.c_str(), application_id.c_str());
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void set_default_component(const Glib::ustring& mime_type, const Glib::ustring& iid) throw(exception)
#else
void set_default_component(const Glib::ustring& mime_type, const Glib::ustring& iid, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GnomeVFSResult result = gnome_vfs_mime_set_default_component(mime_type.c_str(), iid.c_str());
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void set_icon(const Glib::ustring& mime_type, const Glib::ustring& filename) throw(exception)
#else
void set_icon(const Glib::ustring& mime_type, const Glib::ustring& filename, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GnomeVFSResult result = gnome_vfs_mime_set_icon(mime_type.c_str(), filename.c_str());
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void set_description(const Glib::ustring& mime_type, const Glib::ustring& description) throw(exception)
#else
void set_description(const Glib::ustring& mime_type, const Glib::ustring& description, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GnomeVFSResult result = gnome_vfs_mime_set_description(mime_type.c_str(), description.c_str());
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void set_can_be_executable(const Glib::ustring& mime_type, bool new_value) throw(exception)
#else
void set_can_be_executable(const Glib::ustring& mime_type, bool new_value, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GnomeVFSResult result = gnome_vfs_mime_set_can_be_executable(mime_type.c_str(), new_value);
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void set_short_list_applications(const Glib::ustring& mime_type, ListHandleStrings& application_ids) throw(exception)
#else
void set_short_list_applications(const Glib::ustring& mime_type, ListHandleStrings& application_ids, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GnomeVFSResult result = gnome_vfs_mime_set_short_list_applications(mime_type.c_str(), application_ids.data());
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void set_short_list_components(const Glib::ustring& mime_type, ListHandleStrings& component_iids) throw(exception)
#else
void set_short_list_components(const Glib::ustring& mime_type, ListHandleStrings& component_iids, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GnomeVFSResult result = gnome_vfs_mime_set_short_list_components(mime_type.c_str(), component_iids.data());
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void add_application_to_short_list(const Glib::ustring& mime_type, Glib::ustring& application_id) throw(exception)
#else
void add_application_to_short_list(const Glib::ustring& mime_type, Glib::ustring& application_id, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GnomeVFSResult result = gnome_vfs_mime_add_application_to_short_list(mime_type.c_str(), application_id.c_str());
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void remove_application_from_short_list(const Glib::ustring& mime_type, Glib::ustring& application_id) throw(exception)
#else
void remove_application_from_short_list(const Glib::ustring& mime_type, Glib::ustring& application_id, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GnomeVFSResult result = gnome_vfs_mime_remove_application_from_short_list(mime_type.c_str(), application_id.c_str());
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void add_component_to_short_list(const Glib::ustring& mime_type, Glib::ustring& component_iid) throw(exception)
#else
void add_component_to_short_list(const Glib::ustring& mime_type, Glib::ustring& component_iid, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GnomeVFSResult result = gnome_vfs_mime_add_component_to_short_list(mime_type.c_str(), component_iid.c_str());
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void remove_component_from_short_list(const Glib::ustring& mime_type, Glib::ustring& component_iid) throw(exception)
#else
void remove_component_from_short_list(const Glib::ustring& mime_type, Glib::ustring& component_iid, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GnomeVFSResult result = gnome_vfs_mime_remove_component_from_short_list(mime_type.c_str(), component_iid.c_str());
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void add_extension(const Glib::ustring& mime_type, Glib::ustring& extension) throw(exception)
#else
void add_extension(const Glib::ustring& mime_type, Glib::ustring& extension, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GnomeVFSResult result = gnome_vfs_mime_add_extension(mime_type.c_str(), extension.c_str());
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void remove_extension(const Glib::ustring& mime_type, Glib::ustring& extension) throw(exception)
#else
void remove_extension(const Glib::ustring& mime_type, Glib::ustring& extension, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GnomeVFSResult result = gnome_vfs_mime_remove_extension(mime_type.c_str(), extension.c_str());
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void extend_all_applications(const Glib::ustring& mime_type, ListHandleStrings& application_ids) throw(exception)
#else
void extend_all_applications(const Glib::ustring& mime_type, ListHandleStrings& application_ids, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GnomeVFSResult result = gnome_vfs_mime_extend_all_applications(mime_type.c_str(), application_ids.data());
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void remove_from_all_applications(const Glib::ustring& mime_type, ListHandleStrings& application_ids) throw(exception)
#else
void remove_from_all_applications(const Glib::ustring& mime_type, ListHandleStrings& application_ids, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GnomeVFSResult result = gnome_vfs_mime_remove_from_all_applications(mime_type.c_str(), application_ids.data());
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void set_extensions_list(const Glib::ustring& mime_type, const Glib::ustring& extensions_list) throw(exception)
#else
void set_extensions_list(const Glib::ustring& mime_type, const Glib::ustring& extensions_list, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GnomeVFSResult result = gnome_vfs_mime_set_extensions_list(mime_type.c_str(), extensions_list.c_str());
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}

void registered_mime_type_delete(const Glib::ustring& mime_type)
{
  gnome_vfs_mime_registered_mime_type_delete(mime_type.c_str());
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void set_registered_type_key(const Glib::ustring& mime_type, const Glib::ustring& key, const Glib::ustring& data) throw(exception)
#else
void set_registered_type_key(const Glib::ustring& mime_type, const Glib::ustring& key, const Glib::ustring& data, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GnomeVFSResult result = gnome_vfs_mime_set_registered_type_key(mime_type.c_str(), key.c_str(), data.c_str());
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void set_value(const Glib::ustring& mime_type, const Glib::ustring& key, const Glib::ustring& value) throw(exception)
#else
void set_value(const Glib::ustring& mime_type, const Glib::ustring& key, const Glib::ustring& value, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GnomeVFSResult result = gnome_vfs_mime_set_value(mime_type.c_str(), key.c_str(), value.c_str());
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}

void freeze()
{
  gnome_vfs_mime_freeze();
}

void thaw()
{
  gnome_vfs_mime_thaw();
}

void info_reload()
{
  gnome_vfs_mime_info_reload();
}

void reset()
{
  gnome_vfs_mime_reset();
}

} // namespace Mime
} // namespace Vfs
} // namespace Gnome
