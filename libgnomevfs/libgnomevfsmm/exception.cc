#include <libgnomevfsmm/exception.h>

/* Copyright 2003 gnome-vfsmm Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

namespace Gnome
{

namespace Vfs
{

exception::exception()
: gobj_(Gnome::Vfs::OK)
{
}

exception::exception(Gnome::Vfs::Result result)
: gobj_( result )
{
}

exception::~exception() throw()
{
}

Glib::ustring exception::what() const
{
  const char* error = gnome_vfs_result_to_string( static_cast<GnomeVFSResult>(gobj_) );
  if(error)
    return error;
  else
    return Glib::ustring();
}

} // namespace Vfs
} // namespace Gnome
