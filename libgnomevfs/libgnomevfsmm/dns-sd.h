/* Copyright 2003 gnome-vfsmm Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef _LIBGNOMEVFSMM_DNS_SD_H
#define _LIBGNOMEVFSMM_DNS_SD_H

//#include <libgnomevfsmm/async-handle.h>
#include <libgnomevfsmm/enums.h> //For Result
#include <glibmm.h>
#include <libgnomevfs/gnome-vfs-dns-sd.h>



namespace Gnome
{

namespace Vfs
{

//Forward declaration
class exception;

namespace DnsSd
{

//TODO: Hide the C structs?
typedef struct GnomeVFSDNSSDBrowseHandle BrowseHandle;
typedef struct GnomeVFSDNSSDResolveHandle ResolveHandle;


class Service
{
public:
  Glib::ustring name;
  Glib::ustring type;
  Glib::ustring domain;
};

/** For instance,
 * void on_browse(BrowseHandle* handle, GnomeVFSDNSSDServiceStatus status, const Service& service);
 */
typedef sigc::slot<void, BrowseHandle*, GnomeVFSDNSSDServiceStatus, const Service&> BrowseSlot;

//The return value should only be used to call stop_browse().  
#ifdef GLIBMM_EXCEPTIONS_ENABLED
BrowseHandle* browse(const Glib::ustring& domain, const Glib::ustring& type, const BrowseSlot& slot);
#else
BrowseHandle* browse(const Glib::ustring& domain, const Glib::ustring& type, const BrowseSlot& slot, std::auto_ptr<Gnome::Vfs::exception>& error);
#endif //GLIBMM_EXCEPTIONS_ENABLED

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void stop_browse(BrowseHandle* handle);
#else
void stop_browse(BrowseHandle* handle, std::auto_ptr<Gnome::Vfs::exception>& error);
#endif //GLIBMM_EXCEPTIONS_ENABLED

typedef std::map<Glib::ustring, Glib::ustring> ServiceOptions;

/** For instance,
 * void on_resolve(ResolveHandle* handle, Result result, const Service& service, const Glib::ustring& host, int port,
  const ServiceOptions& options, int text_raw_len, const char *text_raw);
 */
typedef sigc::slot<void, ResolveHandle*, Result, const Service&, const Glib::ustring&, int,
  const ServiceOptions&> ResolveSlot;

//The return value should only be used to call cancel_resolve().
#ifdef GLIBMM_EXCEPTIONS_ENABLED
ResolveHandle* resolve(const Glib::ustring& name, const Glib::ustring& type, const Glib::ustring& domain,
                       int timeout, const ResolveSlot& slot);
#else
ResolveHandle* resolve(const Glib::ustring& name, const Glib::ustring& type, const Glib::ustring& domain,
                       int timeout, const ResolveSlot& slot, std::auto_ptr<Gnome::Vfs::exception>& error);
#endif //GLIBMM_EXCEPTIONS_ENABLED

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void cancel_resolve(ResolveHandle* handle);
#else
void cancel_resolve(ResolveHandle* handle, std::auto_ptr<Gnome::Vfs::exception>& error);
#endif //GLIBMM_EXCEPTIONS_ENABLED
  

typedef Glib::ListHandle<Service> ListHandleServices;

//TODO: Use a generic Glib *Handle instead of std::list<Service>?
#ifdef GLIBMM_EXCEPTIONS_ENABLED
void browse_sync(const Glib::ustring& domain, const Glib::ustring& type, int timeout_msec, std::list<Service>& services);
#else
void browse_sync(const Glib::ustring& domain, const Glib::ustring& type, int timeout_msec, std::list<Service>& services, std::auto_ptr<Gnome::Vfs::exception>& error);
#endif //GLIBMM_EXCEPTIONS_ENABLED


#ifdef GLIBMM_EXCEPTIONS_ENABLED
void resolve_sync(const Glib::ustring& name, const Glib::ustring& type, const Glib::ustring& domain,
                  int timeout_msec, Glib::ustring& host, int& port, ServiceOptions& options);
#else
void resolve_sync(const Glib::ustring& name, const Glib::ustring& type, const Glib::ustring& domain,
                  int timeout_msec, Glib::ustring& host, int& port, ServiceOptions& options, std::auto_ptr<Gnome::Vfs::exception>& error);
#endif //GLIBMM_EXCEPTIONS_ENABLED

typedef Glib::ListHandle<Glib::ustring> ListHandleStrings;

#ifdef GLIBMM_EXCEPTIONS_ENABLED
ListHandleStrings list_browse_domains_sync(const Glib::ustring& domain, int timeout_msec);
#else
ListHandleStrings list_browse_domains_sync(const Glib::ustring& domain, int timeout_msec, std::auto_ptr<Gnome::Vfs::exception>& error);
#endif //GLIBMM_EXCEPTIONS_ENABLED

ListHandleStrings get_default_browse_domains();


} // namespace DnsSd

} // namespace Vfs

} // namespace Gnome

#endif
