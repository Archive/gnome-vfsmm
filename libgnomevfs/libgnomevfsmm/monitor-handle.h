#ifndef _LIBGNOMEVFSMM_MONITOR_HANDLE_H
#define _LIBGNOMEVFSMM_MONITOR_HANDLE_H

#include <glibmm.h>

/* Copyright 2003 gnome-vfsmm Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <libgnomevfsmm/enums.h>
#include <libgnomevfsmm/exception.h>
#include <libgnomevfs/gnome-vfs-monitor.h>


#ifndef DOXYGEN_SHOULD_SKIP_THIS
typedef struct GnomeVFSMonitorHandle GnomeVFSMonitorHandle;
#endif /* DOXYGEN_SHOULD_SKIP_THIS */


namespace Gnome
{

namespace Vfs
{

#ifndef DOXYGEN_SHOULD_SKIP_THIS
namespace
{
  
class SignalProxy_Monitor;

}
#endif /* DOXYGEN_SHOULD_SKIP_THIS */

class MonitorHandle
{
public:
  MonitorHandle();
  virtual ~MonitorHandle();

  /// e.g. void on_monitor(const Gnome::Vfs::MonitorHandle& handle, const Glib::ustring& monitor_uri, const Glib::ustring& info_uri, Gnome::Vfs::MonitorEventType type);
  typedef sigc::slot<void, const MonitorHandle&, const Glib::ustring&, const Glib::ustring&, MonitorEventType> SlotMonitor;

#ifdef GLIBMM_EXCEPTIONS_ENABLED
  void add(const Glib::ustring& text_uri, MonitorType type, const SlotMonitor& slot) throw(exception);
  void cancel() throw(exception);
#else
  void add(const Glib::ustring& text_uri, MonitorType type, const SlotMonitor& slot, std::auto_ptr<Gnome::Vfs::exception>& error);
  void cancel(std::auto_ptr<Gnome::Vfs::exception>& error);
#endif //GLIBMM_EXCEPTIONS_ENABLED
  
  GnomeVFSMonitorHandle** gobj_addr();
  GnomeVFSMonitorHandle* gobj();
  const GnomeVFSMonitorHandle* gobj() const;
    
private:

  GnomeVFSMonitorHandle* gobj_;
  SignalProxy_Monitor* proxy_;
};

} // namespace Vfs
} // namespace Gnome

#endif /* _LIBGNOMEVFSMM_MONITOR_HANDLE_H */

