/* $Id$ */

/* Copyright 2003 gnome-vfsmm Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#ifndef _LIBGNOMEVFSMM_DIRECTORYHANDLE_H
#define _LIBGNOMEVFSMM_DIRECTORYHANDLE_H

#include <glibmm.h>

#include <libgnomevfsmm/uri.h>
#include <libgnomevfsmm/file-info.h>
#include <libgnomevfsmm/enums.h>
#include <libgnomevfsmm/exception.h>
#include <libgnomevfs/gnome-vfs-directory.h>


#ifndef DOXYGEN_SHOULD_SKIP_THIS
typedef struct GnomeVFSDirectoryHandle GnomeVFSDirectoryHandle;
#endif /* DOXYGEN_SHOULD_SKIP_THIS */

namespace Gnome
{

namespace Vfs
{

class DirectoryHandle
{
public:
  DirectoryHandle();
  virtual ~DirectoryHandle();


  /// e.g. bool on_visit(const Glib::ustring& rel_path, const Glib::RefPtr<const FileInfo>& info, bool recursing_will_loop, bool& recurse);
  typedef sigc::slot<bool, const Glib::ustring&, const Glib::RefPtr<const FileInfo>&, bool, bool&> SlotVisit;

  /* 
  typedef gboolean (* GnomeVFSDirectoryVisitFunc)	 (const gchar *rel_path,
						  GnomeVFSFileInfo *info,
						  gboolean recursing_will_loop,
						  gpointer data,
						  gboolean *recurse);
  */
              
#ifdef GLIBMM_EXCEPTIONS_ENABLED
  static void visit(const Glib::ustring& uri, FileInfoOptions info_options, DirectoryVisitOptions visit_options, const SlotVisit& slot) throw(exception);
  static void visit(const Glib::RefPtr<const Uri>& uri, FileInfoOptions info_options, DirectoryVisitOptions visit_options, const SlotVisit& slot) throw(exception);
 
  static void visit_files(const Glib::ustring& uri, const Glib::ListHandle<Glib::ustring>& file_list, FileInfoOptions info_options, DirectoryVisitOptions visit_options, const SlotVisit& slot) throw(exception);
  static void visit_files(const Glib::RefPtr<const Uri>& uri, const Glib::ListHandle<Glib::ustring> & file_list, FileInfoOptions info_options, DirectoryVisitOptions visit_options, const SlotVisit& slot) throw(exception);

  static void list_load(const Glib::ListHandle<Glib::ustring>& list, const Glib::ustring& text_uri, FileInfoOptions info_options) throw(exception);

  void open(const Glib::ustring& text_uri, FileInfoOptions options = FILE_INFO_DEFAULT) throw(exception);
  void open(const Glib::RefPtr<const Uri>& uri, FileInfoOptions options = FILE_INFO_DEFAULT) throw(exception);

  void close() throw(exception);

  Glib::RefPtr<FileInfo> read_next(bool& file_exists) throw(exception);

  /** @result If the file does not exist then this will be false.
   */
  Glib::RefPtr<FileInfo> read_next() throw(exception);
#else
  static void visit(const Glib::ustring& uri, FileInfoOptions info_options, DirectoryVisitOptions visit_options, const SlotVisit& slot, std::auto_ptr<Gnome::Vfs::exception>& error);
  static void visit(const Glib::RefPtr<const Uri>& uri, FileInfoOptions info_options, DirectoryVisitOptions visit_options, const SlotVisit& slot, std::auto_ptr<Gnome::Vfs::exception>& error);
 
  static void visit_files(const Glib::ustring& uri, const Glib::ListHandle<Glib::ustring>& file_list, FileInfoOptions info_options, DirectoryVisitOptions visit_options, const SlotVisit& slot, std::auto_ptr<Gnome::Vfs::exception>& error);
  static void visit_files(const Glib::RefPtr<const Uri>& uri, const Glib::ListHandle<Glib::ustring> & file_list, FileInfoOptions info_options, DirectoryVisitOptions visit_options, const SlotVisit& slot, std::auto_ptr<Gnome::Vfs::exception>& error);

  static void list_load(const Glib::ListHandle<Glib::ustring>& list, const Glib::ustring& text_uri, FileInfoOptions info_options, std::auto_ptr<Gnome::Vfs::exception>& error);

  void open(const Glib::ustring& text_uri, FileInfoOptions options, std::auto_ptr<Gnome::Vfs::exception>& error);
  void open(const Glib::RefPtr<const Uri>& uri, FileInfoOptions options, std::auto_ptr<Gnome::Vfs::exception>& error);

  void close(std::auto_ptr<Gnome::Vfs::exception>& error);

  Glib::RefPtr<FileInfo> read_next(bool& file_exists, std::auto_ptr<Gnome::Vfs::exception>& error);

  /** @result If the file does not exist then this will be false.
   */
  Glib::RefPtr<FileInfo> read_next(std::auto_ptr<Gnome::Vfs::exception>& error);
#endif //GLIBMM_EXCEPTIONS_ENABLED
  
  GnomeVFSDirectoryHandle* gobj();
  const GnomeVFSDirectoryHandle* gobj() const;  


protected:
 
  GnomeVFSDirectoryHandle* gobj_;
};

} // namespace Vfs
} // namespace Gnome




#endif /* _LIBGNOMEVFSMM_DIRECTORYHANDLE_H */

