/* $Id$ */

/* Copyright 2003 gnome-vfsmm Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#ifndef _LIBGNOMEVFSMM_APPLICATION_REGISTRY_H
#define _LIBGNOMEVFSMM_APPLICATION_REGISTRY_H

#include <glibmm.h>

#include <libgnomevfsmm/enums.h>
#include <libgnomevfsmm/exception.h>
#include <libgnomevfsmm/mime-application.h>
#include <libgnomevfs/gnome-vfs-application-registry.h>


namespace Gnome
{

namespace Vfs
{

//TODO: Some explanation of how/when this might be used.
//TODO: There seems to be some duplication of functinoality between here and the Mime namespace.
//At first glance it seems like something that would be used by the GNOME file-types control panel, and nothing else. Murray.
namespace ApplicationRegistry
{

typedef Glib::ListHandle<Glib::ustring> ListHandleStrings;


/** Check to see if there is an entry for @a app_id in the registry.
 * See Gnome::Vfs::Mime or get_applications() for information on how to obtain an application ID.
 * @param app_id A string containing an application ID.
 * @return <tt>true</tt> if there is an entry, otherwise <tt>false</tt>.
 */
bool exists(const Glib::ustring& app_id);

//TODO: Explain what a key is? Why does each application have keys? Murray.

/** Get a list of keys for a specified application in the registry.
 * See Gnome::Vfs::Mime or get_applications() for information on how to obtain an application ID.
 * @param app_id A string containing an application ID.
 * @return A list of strings containing keys set for @a app_id .
 */
ListHandleStrings get_keys(const Glib::ustring& app_id);

/** Get the value associated with @a key for @a app_id in the registry.
 * See Gnome::Vfs::Mime or get_applications() for information on how to obtain an application ID.
 * @param app_id A string containing an application ID.
 * @param key A string containing the key to look up.
 * @return A string containing the value assosciated with the key or an empty string if there is no associated value.
 */
Glib::ustring peek_value(const Glib::ustring& app_id, const Glib::ustring& key);

//TODO: "the structure pointed to by @a app_id" doesn't make much sense here. Is it an application or a structure? Murray.
//TODO: This looks like a simple convenience method. If so, it should say so.
//TODO: Why does this have the got_key() method, but peek_value() doesn't.
//TODO: Should peek_value() be called get_value()?
/** Look up a key in the structure pointed to by @a app_id and return the boolean value of that key.
 * See Gnome::Vfs::Mime or get_applications() for information on how to obtain an application ID.
 * @param app_id A string containing an application ID.
 * @param key A string containing the key to look up.
 * @param got_key An output parameter that will be set to <tt>true</tt> if a setting was found, otherwise <tt>false</tt>.
 * @return <tt>true</tt> if @a key is set to &quot;true&quot; or &quot;yes&quot; for @a app_id, otherwise <tt>false</tt>.
 */
bool get_bool_value(const Glib::ustring& app_id, const Glib::ustring& key, bool& got_key);

/** Remove the application represented by @a app_id from the registry.
 * See Gnome::Vfs::Mime or get_applications() for information on how to obtain an application ID.
 * You will need to call sync() to save the changes.
 * @param app_id A string containing an application ID.
 */
void remove_application(const Glib::ustring& app_id);

/** Set values pertaining to the registry entry pointed to by @a app_id.
 * See Gnome::Vfs::Mime or get_applications() for information on how to obtain an application ID.
 * You will need to call sync() to save the changes.
 * @param app_id A string containing the registry ID of the application.
 * @param key A string containing the target key.
 * @param value A string containing the value to set the target key to.
 */
void set_value(const Glib::ustring& app_id, const Glib::ustring& key, const Glib::ustring& value);

//TODO: Consider calling sync() automatically - we need to investigate possible performance problems of doing this. Murray.
//TODO: The default argument here is only appropriate if a bool is more likely to be used than any other type,
//      and if so we should have bool get_value() and string get_string_value().
/** Set values pertaining to the registry entry pointed to by @a app_id.
 * See Gnome::Vfs::Mime or get_applications() for information on how to obtain an application ID.
 * You will need to call sync() to save the changes.
 * @param app_id A string containing the registry ID of the application.
 * @param key A string containing the target key.
 * @param value A boolean containing the value to set the target key to.
 */
void set_value(const Glib::ustring& app_id, const Glib::ustring& key, bool value = true);

//TODO: Say what would happen when get_value() is then used.
/** Wipe the current value that @a key contains for @a app_id.
 * See Gnome::Vfs::Mime or get_applications() for information on how to obtain an application ID.
 * You will need to call sync() to save the changes.
 * @param app_id A string containing the registry ID of the application.
 * @param key A string containing the target key.
 */
void unset_key(const Glib::ustring& app_id, const Glib::ustring& key);

/*
 * Query functions
 */

/** Get all application IDs from the registry that are associated with the given MIME type.
 * If @a mime_type is NULL, it returns all applications.
 * @param mime_type A string containing an MIME type.
 * @return A list of strings containing application IDs.
 */
ListHandleStrings get_applications(const Glib::ustring& mime_type);

/** Get all MIME types that can be handled by an application.
 * See Gnome::Vfs::Mime or get_applications() for information on how to obtain an application ID.
 * @param app_id A string containing an application ID.
 * @return A list of strings containing MIME types.
 */
ListHandleStrings get_mime_types(const Glib::ustring& app_id);

/** Checks whether an application is associated with a given MIME type.
 * See Gnome::Vfs::Mime or get_applications() for information on how to obtain an application ID.
 * @param app_id A string containing an application ID.
 * @param mime_type A string containing an MIME type.
 * @return <tt>true</tt> if @a app_id supports @a mime_type , otherwise <tt>false</tt>.
 */
bool supports_mime_type(const Glib::ustring& app_id, const Glib::ustring& mime_type);

/** Checks whether an application supports the given URI scheme.
 * See Gnome::Vfs::Mime or get_applications() for information on how to obtain an application ID.
 * See Gnome::Vfs::MimeApplication for information on how to obtain a URI scheme.
 * @param app_id A string containing an application ID.
 * @param uri_scheme A string containing a URI scheme.
 * @return <tt>true</tt> if @a app_id supports @a uri_scheme , otherwise <tt>false</tt>.
 */
bool supports_uri_scheme(const Glib::ustring& app_id, const Glib::ustring& uri_scheme);

/*
 * Mime type functions
 * Note that mime_type can be a specific(image/png) or generic (image/<star>) type
 */

/** Remove the MIME types associated with an application.
 * See Gnome::Vfs::Mime or get_applications() for information on how to obtain an application ID.
 * You will need to call sync() to save the changes.
 * @param app_id A string containing an application ID.
 */
void clear_mime_types(const Glib::ustring& app_id);

/** Associate an MIME type with an application.
 * See Gnome::Vfs::Mime or get_applications() for information on how to obtain an application ID.
 * You will need to call sync() to save the changes.
 * @param app_id A string containing an application ID.
 * @param mime_type A string containing an MIME type.
 */
void add_mime_type(const Glib::ustring& app_id, const Glib::ustring& mime_type);

/** De-Associate an MIME type from an application.
 * See Gnome::Vfs::Mime or get_applications() for information on how to obtain an application ID.
 * You will need to call sync() to save the changes.
 * @param app_id A string containing an application ID.
 * @param mime_type A string containing an MIME type.
 */
void remove_mime_type(const Glib::ustring& app_id, const Glib::ustring& mime_type);


/** This function will sync the registry.
 * Typically you would use this function after a modification of the registry.  When you modify the registry, a dirty flag is set.  Calling this function will save your modifications to disk and reset the flag.
 */
#ifdef GLIBMM_EXCEPTIONS_ENABLED
void sync() throw(exception);
#else
void sync(std::auto_ptr<Gnome::Vfs::exception>& error);
#endif

//TODO: What does "free resources" mean. Are we blocking another application from doing the same thing?
//      How is this different to sync()? Do we need to do both? Murray
/** Synchronize the application registry data to disk and free resources.
 */
void shutdown();

//TODO: So, when would we need to call this? Murray.
/** If this function is called for the first time, it will initialize the registry.
 * Subsequent calls to the function will clear out the current registry contents and load registry contents from the save file.  Make certain that you have saved your registry before calling this function.  It will destroy unsaved changes!!
 */
void reload();

/*
 * Integrating with gnome-vfs-mime-handlers
 */

//TODO: How is a MimeApplication different to an Application? Murray.
/** Get the MimeApplication associated with @a app_id .
 * See Gnome::Vfs::Mime or get_applications() for information on how to obtain an application ID.
 * @param app_id A string containing an application ID.
 * @return The MimeApplication object associated with the application ID.
 */
MimeApplication get_mime_application(const Glib::ustring& app_id);

/** Save an application to the registry that will be associated with a defined MIME type.
 * @param application The application associated with the MIME type.
 */
void save_mime_application(const MimeApplication& application);

} //namespace ApplicationRegistry

} // namespace Vfs
} // namespace Gnome




#endif /* _LIBGNOMEVFSMM_APPLICATION_REGISTRY_H */

