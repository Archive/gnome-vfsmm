/* $Id$ */

/* Copyright 2003 gnome-vfsmm Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <libgnomevfsmm/application-registry.h>
#include <libgnomevfsmm/private.h>

namespace Gnome
{

namespace Vfs
{

namespace ApplicationRegistry
{


/***
 * Existance check
 */
bool exists(const Glib::ustring& app_id)
{
  return gnome_vfs_application_registry_exists(app_id.c_str());
}


ListHandleStrings get_keys(const Glib::ustring& app_id)
{
  GList* pList = gnome_vfs_application_registry_get_keys(app_id.c_str());
  return Glib::ListHandle<Glib::ustring>(pList, Glib::OWNERSHIP_SHALLOW);
}

Glib::ustring peek_value(const Glib::ustring& app_id, const Glib::ustring& key)
{
  return gnome_vfs_application_registry_peek_value(app_id.c_str(), key.c_str());
}

bool get_bool_value(const Glib::ustring& app_id, const Glib::ustring& key, bool& got_key)
{
  gboolean c_got_key = FALSE;
  return gnome_vfs_application_registry_get_bool_value(app_id.c_str(), key.c_str(), &c_got_key);
  got_key = c_got_key;
}

void remove_application(const Glib::ustring& app_id)
{
  gnome_vfs_application_registry_remove_application(app_id.c_str());
}

void set_value(const Glib::ustring& app_id, const Glib::ustring& key, const Glib::ustring& value)
{
  gnome_vfs_application_registry_set_value(app_id.c_str(), key.c_str(), value.c_str());
}

void set_value(const Glib::ustring& app_id, const Glib::ustring& key, bool value)
{
  gnome_vfs_application_registry_set_bool_value(app_id.c_str(), key.c_str(), value);
}

void unset_key(const Glib::ustring& app_id, const Glib::ustring& key)
{
  gnome_vfs_application_registry_unset_key(app_id.c_str(), key.c_str());
}

ListHandleStrings get_applications(const Glib::ustring& mime_type)
{
  GList* pList = gnome_vfs_application_registry_get_applications(mime_type.c_str());
  return Glib::ListHandle<Glib::ustring>(pList, Glib::OWNERSHIP_SHALLOW);
}

ListHandleStrings get_mime_types(const Glib::ustring& app_id)
{
  GList* pList = gnome_vfs_application_registry_get_mime_types(app_id.c_str());
  return Glib::ListHandle<Glib::ustring>(pList, Glib::OWNERSHIP_SHALLOW);
}

bool supports_mime_type(const Glib::ustring& app_id, const Glib::ustring& mime_type)
{
  return gnome_vfs_application_registry_supports_mime_type(app_id.c_str(), mime_type.c_str());
}

bool supports_uri_scheme(const Glib::ustring& app_id, const Glib::ustring& uri_scheme)
{
  return gnome_vfs_application_registry_supports_uri_scheme(app_id.c_str(), uri_scheme.c_str());
}

void clear_mime_types(const Glib::ustring& app_id)
{
  gnome_vfs_application_registry_clear_mime_types(app_id.c_str());
}

void add_mime_type(const Glib::ustring& app_id, const Glib::ustring& mime_type)
{
  gnome_vfs_application_registry_add_mime_type(app_id.c_str(), mime_type.c_str());
}

void remove_mime_type(const Glib::ustring& app_id, const Glib::ustring& mime_type)
{
  gnome_vfs_application_registry_remove_mime_type(app_id.c_str(), mime_type.c_str());
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void sync() throw(exception)
#else
void sync(std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GnomeVFSResult result = gnome_vfs_application_registry_sync();
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}


void shutdown()
{
  gnome_vfs_application_registry_shutdown();
}

void reload()
{
  gnome_vfs_application_registry_reload();
}


MimeApplication get_mime_application(const Glib::ustring& app_id)
{
  return Glib::wrap(gnome_vfs_application_registry_get_mime_application(app_id.c_str()));
}

void save_mime_application(const MimeApplication& application)
{
  gnome_vfs_application_registry_save_mime_application(application.gobj());
}

} //namespace ApplicationRegistry


} //namespace Vfs

} //namespace Gnome
