/* Copyright 2003 gnome-vfsmm Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <libgnomevfsmm/dns-sd.h>
#include <libgnomevfsmm/private.h>
//#include <libgnomevfs/gnome-vfs-dns-sd.h>


namespace Gnome
{

namespace Vfs
{

namespace DnsSd
{

namespace
{

class SignalProxy_Browse
{
public:
  typedef BrowseSlot SlotType;

  SignalProxy_Browse(const SlotType& slot);
  ~SignalProxy_Browse();

  static void c_callback(GnomeVFSDNSSDBrowseHandle* handle, GnomeVFSDNSSDServiceStatus status, const GnomeVFSDNSSDService* service, gpointer callback_data);
  static void c_callback_destroy(gpointer callback_data);

protected:
  SlotType slot_;
};

SignalProxy_Browse::SignalProxy_Browse(const SlotType& slot)
: slot_(slot)
{}

SignalProxy_Browse::~SignalProxy_Browse()
{}

void SignalProxy_Browse::c_callback(GnomeVFSDNSSDBrowseHandle* handle, GnomeVFSDNSSDServiceStatus status, const GnomeVFSDNSSDService* service, gpointer callback_data)
{
  SignalProxy_Browse *const self = static_cast<SignalProxy_Browse*>(callback_data);

  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  try
  {
  #endif //GLIBMM_EXCEPTIONS_ENABLED
    Service cppService;
    if(service)
    {
      cppService.name = Glib::convert_const_gchar_ptr_to_ustring(service->name);
      cppService.type = Glib::convert_const_gchar_ptr_to_ustring(service->type);
      cppService.domain = Glib::convert_const_gchar_ptr_to_ustring(service->domain);
    }
    
    (self->slot_)(handle, status, cppService);
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  }
  catch(...)
  {
    Glib::exception_handlers_invoke();
  }
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}

void SignalProxy_Browse::c_callback_destroy(gpointer callback_data)
{
  delete static_cast<SignalProxy_Browse*>(callback_data);
}


class SignalProxy_Resolve
{
public:
  typedef ResolveSlot SlotType;

  SignalProxy_Resolve(const SlotType& slot);
  ~SignalProxy_Resolve();

  static void c_callback(GnomeVFSDNSSDResolveHandle* handle, GnomeVFSResult result, const GnomeVFSDNSSDService* service, const char* host, int port, const GHashTable* text, int text_raw_len, const char *text_raw, gpointer callback_data);
  static void c_callback_destroy(gpointer callback_data);

protected:
  SlotType slot_;
};

SignalProxy_Resolve::SignalProxy_Resolve(const SlotType& slot)
: slot_(slot)
{}

SignalProxy_Resolve::~SignalProxy_Resolve()
{}

void hash_table_foreach_to_map(gpointer key, gpointer value, gpointer user_data)
{
  ServiceOptions* pMap = (ServiceOptions*)user_data;
  if(pMap && key && value)
  {
    (*pMap)[(char*)value] = (char*)value; //Converts to Glib::ustrings.
  }
}
              
void SignalProxy_Resolve::c_callback(GnomeVFSDNSSDResolveHandle* handle, GnomeVFSResult result, const GnomeVFSDNSSDService* service, const char* host, int port, const GHashTable* text, int /* text_raw_len */, const char* /* text_raw */, gpointer callback_data)
{
  SignalProxy_Resolve *const self = static_cast<SignalProxy_Resolve*>(callback_data);

  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  try
  {
  #endif //GLIBMM_EXCEPTIONS_ENABLED
    Service cppService;
    if(service)
    {
      cppService.name = Glib::convert_const_gchar_ptr_to_ustring(service->name);
      cppService.type = Glib::convert_const_gchar_ptr_to_ustring(service->type);
      cppService.domain = Glib::convert_const_gchar_ptr_to_ustring(service->domain);
    }
      
    //Parse the HashTable and put its items in a std::map:
    ServiceOptions options;
    g_hash_table_foreach(const_cast<GHashTable*>(text), &hash_table_foreach_to_map, &options);
    

    //We ignore the text_raw and text_len arguments, because
    //1. They are not very interesting - the same info is in options, in a structured form.
    //2. They are raw data - an opaque block of bytes.
    //3. libsigc++ can not support that many slot parameters.
    Glib::ustring strHost = Glib::convert_const_gchar_ptr_to_ustring(host);
    (self->slot_)(handle, (Result)result, cppService, strHost, port,
                  options);
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  }
  catch(...)
  {
    Glib::exception_handlers_invoke();
  }
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}

void SignalProxy_Resolve::c_callback_destroy(gpointer callback_data)
{
  delete static_cast<SignalProxy_Resolve*>(callback_data);
}


} // Anonymous namespace


#ifdef GLIBMM_EXCEPTIONS_ENABLED
BrowseHandle* browse(const Glib::ustring& domain, const Glib::ustring& type, const BrowseSlot& slot)
#else
BrowseHandle* browse(const Glib::ustring& domain, const Glib::ustring& type, const BrowseSlot& slot, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  BrowseHandle* handle = 0;

  SignalProxy_Browse* proxy = new SignalProxy_Browse(slot); //It will be deleted by the destroy callback.
  
  GnomeVFSResult result = gnome_vfs_dns_sd_browse(&handle, domain.c_str(), type.c_str(), &SignalProxy_Browse::c_callback, proxy, &SignalProxy_Browse::c_callback_destroy);
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED

  return handle;
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void stop_browse(BrowseHandle* handle)
#else
void stop_browse(BrowseHandle* handle, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GnomeVFSResult result = gnome_vfs_dns_sd_stop_browse(handle);
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
ResolveHandle* resolve(const Glib::ustring& name, const Glib::ustring& type, const Glib::ustring& domain,
                       int timeout, const ResolveSlot& slot)
#else
ResolveHandle* resolve(const Glib::ustring& name, const Glib::ustring& type, const Glib::ustring& domain,
                       int timeout, const ResolveSlot& slot, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{  
  ResolveHandle* handle = 0;

  SignalProxy_Resolve* proxy = new SignalProxy_Resolve(slot); //It will be deleted by the destroy callback.

  GnomeVFSResult result = gnome_vfs_dns_sd_resolve(&handle, name.c_str(), type.c_str(), domain.c_str(), timeout,
    &SignalProxy_Resolve::c_callback, proxy, &SignalProxy_Resolve::c_callback_destroy);
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED

  return handle;
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void cancel_resolve(ResolveHandle* handle)
#else
void cancel_resolve(ResolveHandle* handle, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  //GnomeVFSResult result = gnome_vfs_dns_sd_cancel_resolve(handle);
  //handle_result(result);
}


#ifdef GLIBMM_EXCEPTIONS_ENABLED
void browse_sync(const Glib::ustring& domain, const Glib::ustring& type, int timeout_msec, std::list<Service>& services)
#else
void browse_sync(const Glib::ustring& domain, const Glib::ustring& type, int timeout_msec, std::list<Service>& services, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GnomeVFSDNSSDService* c_services = 0;
  int n_c_services = 0;
  GnomeVFSResult result = gnome_vfs_dns_sd_browse_sync(domain.c_str(), type.c_str(), timeout_msec,
    &n_c_services, &c_services);
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  if(error.get())
    return;
  #endif //GLIBMM_EXCEPTIONS_ENABLED

  //Put the service details in a C++ container, and release the C list:
  if(c_services)
  {
    std::list<Service> listServices;
    for(int i = 0; i < n_c_services; ++i)
    {
      const GnomeVFSDNSSDService& service = c_services[i];

      Service cppService;
      cppService.name = Glib::convert_const_gchar_ptr_to_ustring(service.name);
      cppService.type = Glib::convert_const_gchar_ptr_to_ustring(service.type);
      cppService.domain = Glib::convert_const_gchar_ptr_to_ustring(service.domain);
      
      listServices.push_back(cppService);
    }

    gnome_vfs_dns_sd_service_list_free(c_services, n_c_services);
    services = listServices; //Copy specific container into generic container.
  }
                          
}


#ifdef GLIBMM_EXCEPTIONS_ENABLED
void resolve_sync(const Glib::ustring& name, const Glib::ustring& type, const Glib::ustring& domain,
                  int timeout_msec, Glib::ustring& host, int& port, ServiceOptions& options)
#else
void resolve_sync(const Glib::ustring& name, const Glib::ustring& type, const Glib::ustring& domain,
                  int timeout_msec, Glib::ustring& host, int& port, ServiceOptions& options, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  char* c_host = 0;
  GHashTable* cOptions = 0;
  char* text_raw = 0; //We ignore this. See above.
  int text_raw_len = 0; //We ignore this. See above.
  GnomeVFSResult result = gnome_vfs_dns_sd_resolve_sync(name.c_str(), type.c_str(), domain.c_str(), timeout_msec, 
    &c_host, &port, &cOptions, &text_raw_len, &text_raw);
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  if(error.get())
    return;
  #endif //GLIBMM_EXCEPTIONS_ENABLED

  host = Glib::convert_const_gchar_ptr_to_ustring(c_host);
  
  //Put the service options in the C++ map:
  options.clear(); //Initialize it.
  g_hash_table_foreach(const_cast<GHashTable*>(cOptions), &hash_table_foreach_to_map, &options);

  //Free the C output args:
  g_free(c_host);
  c_host = 0;

  g_hash_table_destroy(cOptions);

  g_free(text_raw);
  text_raw = 0;
}


#ifdef GLIBMM_EXCEPTIONS_ENABLED
ListHandleStrings list_browse_domains_sync(const Glib::ustring& domain, int timeout_msec)
#else
ListHandleStrings list_browse_domains_sync(const Glib::ustring& domain, int timeout_msec, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GList* clist = 0;
  GnomeVFSResult result = gnome_vfs_dns_sd_list_browse_domains_sync(domain.c_str(), timeout_msec, &clist);
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED

  return Glib::ListHandle<Glib::ustring>(clist, Glib::OWNERSHIP_SHALLOW); //TODO: Check ownership.
}

ListHandleStrings get_default_browse_domains()
{
  GList* clist = gnome_vfs_get_default_browse_domains();
  return Glib::ListHandle<Glib::ustring>(clist, Glib::OWNERSHIP_SHALLOW); //TODO: Check ownership.
}

} //namespace DnsSd

} //namespace Vfs

} //namespace Gnome



