/* Copyright 2003, 2006 gnome-vfsmm Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <libgnomevfsmm/async-2.h>
#include <libgnomevfs/gnome-vfs-job-limit.h>

namespace Gnome
{

namespace Vfs
{

namespace Async
{

void set_job_limit(int limit)
{
  Async2::set_job_limit(limit);
}

int get_job_limit()
{
  return Async2::get_job_limit();
}

} //namespace Async

} //namespace Vfs

} //namespace Gnome



