/* $Id$ */               

/* Copyright 2003 gnome-vfsmm Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <libgnomevfsmm/directory-handle.h>
#include <libgnomevfsmm/private.h>

#include <libgnomevfs/gnome-vfs-directory.h>

namespace
{

//SignalProxy_Visit:

//This Signal Proxy allows the C++ coder to specify a sigc::slot instead of a static function.
class SignalProxy_Visit
{
public:
  typedef Gnome::Vfs::DirectoryHandle::SlotVisit SlotType;

  SignalProxy_Visit(const SlotType& slot);
  ~SignalProxy_Visit();

  static gboolean c_callback(const gchar* rel_path, GnomeVFSFileInfo* info, gboolean recursing_will_loop, gpointer data, gboolean* recurse);
              
protected:
  SlotType slot_;
};

SignalProxy_Visit::SignalProxy_Visit(const SlotType& slot)
:
  slot_ (slot)
{}

SignalProxy_Visit::~SignalProxy_Visit()
{}

gboolean SignalProxy_Visit::c_callback(const gchar* rel_path, GnomeVFSFileInfo* info, gboolean recursing_will_loop, gpointer data, gboolean* recurse)
{
  SignalProxy_Visit *const self = static_cast<SignalProxy_Visit*>(data);
  gboolean bResult = false;
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  try
  {
  #endif //GLIBMM_EXCEPTIONS_ENABLED
    bool bRecurse = false;
    Glib::ustring strTemp = Glib::convert_const_gchar_ptr_to_ustring(rel_path);
    bResult = (self->slot_)(strTemp, Glib::wrap(info, true /* take_copy */), recursing_will_loop, bRecurse);
    *recurse = bRecurse;
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  }
  catch(...)
  {
    Glib::exception_handlers_invoke();
  }
  #endif //GLIBMM_EXCEPTIONS_ENABLED

  return bResult;
}

} //anonymous namespace

namespace Gnome
{

namespace Vfs
{

GnomeVFSDirectoryHandle* DirectoryHandle::gobj()
{
  return gobj_;
}

const GnomeVFSDirectoryHandle* DirectoryHandle::gobj() const
{
  return gobj_;
}

DirectoryHandle::DirectoryHandle()
: gobj_(0)
{
}

DirectoryHandle::~DirectoryHandle()
{
  if(gobj())
  {
    #ifdef GLIBMM_EXCEPTIONS_ENABLED
    close();
    #else
    std::auto_ptr<Gnome::Vfs::exception> error;
    close(error);
    #endif //GLIBMM_EXCEPTIONS_ENABLED
  }
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void DirectoryHandle::open(const Glib::ustring& text_uri, FileInfoOptions options) throw(exception)
#else
void DirectoryHandle::open(const Glib::ustring& text_uri, FileInfoOptions options, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif
{
  GnomeVFSResult result = gnome_vfs_directory_open(&gobj_, text_uri.c_str(), static_cast<GnomeVFSFileInfoOptions>(options) );
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void DirectoryHandle::open(const Glib::RefPtr<const Uri>& uri, FileInfoOptions options) throw(exception)
#else
void DirectoryHandle::open(const Glib::RefPtr<const Uri>& uri, FileInfoOptions options, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GnomeVFSResult result = gnome_vfs_directory_open_from_uri(&gobj_, const_cast<GnomeVFSURI*>(uri->gobj()), static_cast<GnomeVFSFileInfoOptions>(options) );
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void DirectoryHandle::close() throw(exception)
#else
void DirectoryHandle::close(std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  if(gobj_)
  {
    GnomeVFSResult result = gnome_vfs_directory_close(gobj());
    #ifdef GLIBMM_EXCEPTIONS_ENABLED
    handle_result(result);
    #else
    handle_result(result, error);
    #endif //GLIBMM_EXCEPTIONS_ENABLED

    gobj_ = 0; //This can't be used after it has been closed. And this also stops a crash in the destructor, if we try to close it again.
  }
}
  
#ifdef GLIBMM_EXCEPTIONS_ENABLED
Glib::RefPtr<FileInfo> DirectoryHandle::read_next(bool& file_exists) throw(exception)
#else
Glib::RefPtr<FileInfo> DirectoryHandle::read_next(bool& file_exists, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GnomeVFSFileInfo* fileinfo = gnome_vfs_file_info_new();
  GnomeVFSResult result = gnome_vfs_directory_read_next(gobj(), fileinfo);
  Glib::RefPtr<FileInfo> cppFileInfo = Glib::wrap(fileinfo); //Takes ownership, so we don't need to unref fileinfo.
  
  if(result == GNOME_VFS_ERROR_EOF) //there are no more files.
    file_exists = false;  //This isn't really an error. It's just the end of the file list.
  else
    #ifdef GLIBMM_EXCEPTIONS_ENABLED
    handle_result(result);
    #else
    handle_result(result, error);
    #endif //GLIBMM_EXCEPTIONS_ENABLED
    
  return cppFileInfo;
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
Glib::RefPtr<FileInfo> DirectoryHandle::read_next() throw(exception)
#else
Glib::RefPtr<FileInfo> DirectoryHandle::read_next(std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  bool file_exists = true;
#ifdef GLIBMM_EXCEPTIONS_ENABLED
  Glib::RefPtr<FileInfo> refFileInfo = read_next(file_exists);
#else
  Glib::RefPtr<FileInfo> refFileInfo = read_next(file_exists, error);
  if(error.get())
    return refFileInfo;
#endif
  if (!file_exists)
    refFileInfo.clear(); //ref is now null, indicating read failed.

  return refFileInfo;
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void DirectoryHandle::visit(const Glib::ustring& uri, FileInfoOptions info_options, DirectoryVisitOptions visit_options, const SlotVisit& slot) throw(exception)
#else
void DirectoryHandle::visit(const Glib::ustring& uri, FileInfoOptions info_options, DirectoryVisitOptions visit_options, const SlotVisit& slot, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  SignalProxy_Visit proxy(slot);
  GnomeVFSResult result = gnome_vfs_directory_visit(uri.c_str(), static_cast<GnomeVFSFileInfoOptions>(info_options), static_cast<GnomeVFSDirectoryVisitOptions>(visit_options), &SignalProxy_Visit::c_callback, &proxy);
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void DirectoryHandle::visit(const Glib::RefPtr<const Uri>& uri, FileInfoOptions info_options, DirectoryVisitOptions visit_options, const SlotVisit& slot) throw(exception)
#else
void DirectoryHandle::visit(const Glib::RefPtr<const Uri>& uri, FileInfoOptions info_options, DirectoryVisitOptions visit_options, const SlotVisit& slot, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  SignalProxy_Visit proxy(slot);
  GnomeVFSResult result = gnome_vfs_directory_visit_uri(const_cast<GnomeVFSURI*>(uri->gobj()), static_cast<GnomeVFSFileInfoOptions>(info_options), static_cast<GnomeVFSDirectoryVisitOptions>(visit_options), &SignalProxy_Visit::c_callback, &proxy);
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif
}
 
#ifdef GLIBMM_EXCEPTIONS_ENABLED
void DirectoryHandle::visit_files(const Glib::ustring& uri, const Glib::ListHandle<Glib::ustring>& file_list, FileInfoOptions info_options, DirectoryVisitOptions visit_options, const SlotVisit& slot) throw(exception)
#else
void DirectoryHandle::visit_files(const Glib::ustring& uri, const Glib::ListHandle<Glib::ustring>& file_list, FileInfoOptions info_options, DirectoryVisitOptions visit_options, const SlotVisit& slot, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  SignalProxy_Visit proxy(slot);
  GnomeVFSResult result = gnome_vfs_directory_visit_files(uri.c_str(), file_list.data(), static_cast<GnomeVFSFileInfoOptions>(info_options), static_cast<GnomeVFSDirectoryVisitOptions>(visit_options), &SignalProxy_Visit::c_callback, &proxy);
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void DirectoryHandle::visit_files(const Glib::RefPtr<const Uri>& uri, const Glib::ListHandle<Glib::ustring> & file_list, FileInfoOptions info_options, DirectoryVisitOptions visit_options, const SlotVisit& slot) throw(exception)
#else
void DirectoryHandle::visit_files(const Glib::RefPtr<const Uri>& uri, const Glib::ListHandle<Glib::ustring> & file_list, FileInfoOptions info_options, DirectoryVisitOptions visit_options, const SlotVisit& slot, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  SignalProxy_Visit proxy(slot);
   GnomeVFSResult result = gnome_vfs_directory_visit_files_at_uri(const_cast<GnomeVFSURI*>(uri->gobj()), file_list.data(), static_cast<GnomeVFSFileInfoOptions>(info_options), static_cast<GnomeVFSDirectoryVisitOptions>(visit_options), &SignalProxy_Visit::c_callback, &proxy);
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
   handle_result(result);
  #else
   handle_result(result, error);
  #endif
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void DirectoryHandle::list_load(const Glib::ListHandle<Glib::ustring>& list, const Glib::ustring& text_uri, FileInfoOptions info_options) throw(exception)
#else
void DirectoryHandle::list_load(const Glib::ListHandle<Glib::ustring>& list, const Glib::ustring& text_uri, FileInfoOptions info_options, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GList* temp_list = list.data();
  GnomeVFSResult result = gnome_vfs_directory_list_load(&temp_list, text_uri.c_str(), static_cast<GnomeVFSFileInfoOptions>(info_options));
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif
}
  
} //namespace Vfs

} //namespace Gnome



