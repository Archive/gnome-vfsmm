/* Copyright 2003 gnome-vfsmm Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#ifndef _LIBGNOMEVFSMM_PRIVATE_H
#define _LIBGNOMEVFSMM_PRIVATE_H

#include <glibmm.h>

#include <libgnomevfsmm/exception.h>
#include <libgnomevfs/gnome-vfs-result.h>

namespace Gnome
{

namespace Vfs
{

#ifdef GLIBMM_EXCEPTIONS_ENABLED
  void handle_result(GnomeVFSResult result) throw(Gnome::Vfs::exception);
#else
  void handle_result(GnomeVFSResult result, std::auto_ptr<Gnome::Vfs::exception>& error);
#endif //GLIBMM_EXCEPTIONS_ENABLED

} // namespace Vfs
} // namespace Gnome

#endif /* _LIBGNOMEVFSMM_PRIVATE_H */
