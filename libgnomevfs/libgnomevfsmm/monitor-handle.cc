/* Copyright 2003 gnome-vfsmm Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <libgnomevfsmm/monitor-handle.h>
#include <libgnomevfsmm/enums.h>
#include <libgnomevfsmm/private.h>
#include <iostream>

namespace Gnome
{

namespace Vfs
{

namespace //Anonymous namespace
{
//SignalProxy_Monitor

//This Signal Proxy allows the C++ coder to specify a sigc::slot instead of a static function
class SignalProxy_Monitor
{
public:
  typedef MonitorHandle::SlotMonitor SlotType;

  SignalProxy_Monitor(const SlotType& slot, MonitorHandle* monitor_handle);
  ~SignalProxy_Monitor();

  static void c_callback(GnomeVFSMonitorHandle *monitor, const gchar *monitor_uri, const gchar *info_uri, GnomeVFSMonitorEventType event_type, gpointer user_data);
  
protected:
  SlotType slot_;
  MonitorHandle* monitor_handle_;
};

SignalProxy_Monitor::SignalProxy_Monitor(const SlotType& slot, MonitorHandle* monitor_handle)
: slot_(slot), monitor_handle_(monitor_handle)
{}

SignalProxy_Monitor::~SignalProxy_Monitor()
{}

void SignalProxy_Monitor::c_callback(GnomeVFSMonitorHandle* monitor, const gchar* monitor_uri, const gchar* info_uri, GnomeVFSMonitorEventType event_type, gpointer user_data)
{
  SignalProxy_Monitor *const self = static_cast<SignalProxy_Monitor*>(user_data);
  
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  try
  {
  #endif
    const Glib::ustring strMonitorUriTemp = Glib::convert_const_gchar_ptr_to_ustring(monitor_uri);
    const Glib::ustring strInfoUriTemp = Glib::convert_const_gchar_ptr_to_ustring(info_uri);

    //Instead of getting a 2nd C++ wrapper instance, by doing this:
    //Gnome::Vfs::MonitorHandle monitorTemp(monitor, false /* = does not have ownership */); 
    //We get the same instance that we started with:
    const Gnome::Vfs::MonitorHandle* monitorTemp = self->monitor_handle_;
    //We need to do this because, calling cancel on it in the C++ callback, will destroy it's underlying instance,
    //and every wrapping instance would need to know about that.
    //
    //I guess that Glib::wrap() would normally just give us the same instance a 2nd time,
    //but that's not available to us here because it's not a real wrapper.
    
    (self->slot_)(*monitorTemp, strMonitorUriTemp, strInfoUriTemp, static_cast<Gnome::Vfs::MonitorEventType>(event_type));
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  }
  catch(...)
  {
    Glib::exception_handlers_invoke();
  }
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}

} //Anonymous namespace

GnomeVFSMonitorHandle** MonitorHandle::gobj_addr()
{
  return &gobj_;
}

GnomeVFSMonitorHandle* MonitorHandle::gobj()
{
  return gobj_;
}

const GnomeVFSMonitorHandle* MonitorHandle::gobj() const
{
  return gobj_;
}

MonitorHandle::MonitorHandle()
: gobj_(0), proxy_(0)
{}

MonitorHandle::~MonitorHandle()
{
  //Cancel the callback if one was added.
  //This deallocates the underlying instance.
#ifdef GLIBMM_EXCEPTIONS_ENABLED
  cancel();
#else
  std::auto_ptr<Gnome::Vfs::exception> error;
  cancel(error);
#endif
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void MonitorHandle::add(const Glib::ustring& text_uri, MonitorType type, const SlotMonitor& slot) throw(exception)
#else
void MonitorHandle::add(const Glib::ustring& text_uri, MonitorType type, const SlotMonitor& slot, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  if(!proxy_) //Only one callback at a time is allowed.
  {
    proxy_ = new SignalProxy_Monitor(slot, this);
    GnomeVFSResult result = gnome_vfs_monitor_add(gobj_addr(), text_uri.c_str(), static_cast<GnomeVFSMonitorType>(type), &SignalProxy_Monitor::c_callback, proxy_);
    #ifdef GLIBMM_EXCEPTIONS_ENABLED
    handle_result(result);
    #else
    handle_result(result, error);
    #endif //GLIBMM_EXCEPTIONS_ENABLED
  } //TODO: else throw an exception?
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void MonitorHandle::cancel() throw(exception)
#else
void MonitorHandle::cancel(std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  if(proxy_) //Don't cancel something that hasn't been added.
  {
    GnomeVFSResult result = gnome_vfs_monitor_cancel(gobj());
    #ifdef GLIBMM_EXCEPTIONS_ENABLED
    handle_result(result);
    #else
    handle_result(result, error);
    #endif //GLIBMM_EXCEPTIONS_ENABLED
  }
  
  //Forget the slot proxy:
  if(proxy_) //Check again, in case anything has done this in the meantime, though I don't think anything does.
  {
    delete proxy_;
    proxy_ = 0;
  }
}

} //namespace Vfs

} //namespace Gnome
