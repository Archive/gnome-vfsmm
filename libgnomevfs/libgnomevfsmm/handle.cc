/* $Id$ */               

/* Copyright 2003 gnome-vfsmm Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <libgnomevfsmm/handle.h>
#include <libgnomevfsmm/private.h>
#include <libgnomevfs/gnome-vfs-ops.h>

namespace Gnome
{

namespace Vfs
{

GnomeVFSHandle* Handle::gobj()
{
  return gobj_;
}

const GnomeVFSHandle* Handle::gobj() const
{
  return gobj_;
}

Handle::Handle()
: gobj_(0)
{
}

Handle::~Handle()
{
  if(gobj())
#ifdef GLIBMM_EXCEPTIONS_ENABLED
    close();
#else
  {
      std::auto_ptr<Gnome::Vfs::exception> error;
      close(error);
  }
#endif //GLIBMM_EXCEPTIONS_ENABLED
  //TODO: close() it if it's open? murrayc.
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void Handle::open(const Glib::ustring& text_uri, OpenMode open_mode) throw(exception)
#else
void Handle::open(const Glib::ustring& text_uri, OpenMode open_mode, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GnomeVFSResult result = gnome_vfs_open(&gobj_, text_uri.c_str(), static_cast<GnomeVFSOpenMode>(open_mode));
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void Handle::open(const Glib::RefPtr<const Uri>& uri, OpenMode open_mode) throw(exception)
#else
void Handle::open(const Glib::RefPtr<const Uri>& uri, OpenMode open_mode, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GnomeVFSResult result = gnome_vfs_open_uri(&gobj_, const_cast<GnomeVFSURI*>(uri->gobj()), static_cast<GnomeVFSOpenMode>(open_mode));
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void Handle::create(const Glib::ustring& text_uri, OpenMode open_mode, bool exclusive, guint permissions) throw(exception)
#else
void Handle::create(const Glib::ustring& text_uri, OpenMode open_mode, bool exclusive, guint permissions, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GnomeVFSResult result = gnome_vfs_create(&gobj_, text_uri.c_str(), static_cast<GnomeVFSOpenMode>(open_mode), exclusive, permissions);
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void Handle::create(const Glib::RefPtr<const Uri>& uri, OpenMode open_mode, bool exclusive, guint permissions) throw(exception)
#else
void Handle::create(const Glib::RefPtr<const Uri>& uri, OpenMode open_mode, bool exclusive, guint permissions, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GnomeVFSResult result = gnome_vfs_create_uri(&gobj_, const_cast<GnomeVFSURI*>(uri->gobj()), static_cast<GnomeVFSOpenMode>(open_mode), exclusive, permissions);
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void Handle::close() throw(exception)
#else
void Handle::close(std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GnomeVFSResult result = gnome_vfs_close(gobj());
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
  
  gobj_ = 0;
}
  
#ifdef GLIBMM_EXCEPTIONS_ENABLED
FileSize Handle::read(gpointer buffer, FileSize bytes) throw(exception)
#else
FileSize Handle::read(gpointer buffer, FileSize bytes, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GnomeVFSFileSize bytes_read = 0;
  GnomeVFSResult result = gnome_vfs_read(gobj(), buffer, static_cast<GnomeVFSFileSize>(bytes), &bytes_read);

  if(result != GNOME_VFS_ERROR_EOF) //This is not an error. It's just a normal end of the file. Callers should check bytes_read.
  {
    #ifdef GLIBMM_EXCEPTIONS_ENABLED
    handle_result(result);
    #else
    handle_result(result, error);
    #endif //GLIBMM_EXCEPTIONS_ENABLED
  }
  
  return static_cast<FileSize>(bytes_read);
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
FileSize Handle::write(gconstpointer buffer, FileSize bytes) throw(exception)
#else
FileSize Handle::write(gconstpointer buffer, FileSize bytes, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GnomeVFSFileSize bytes_written = 0;
  GnomeVFSResult result = gnome_vfs_write(gobj(), buffer, static_cast<GnomeVFSFileSize>(bytes), &bytes_written);
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
  return static_cast<FileSize>(bytes_written);
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void Handle::seek(SeekPosition whence, FileOffset offset) throw(exception)
#else
void Handle::seek(SeekPosition whence, FileOffset offset, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GnomeVFSResult result = gnome_vfs_seek(gobj(), static_cast<GnomeVFSSeekPosition>(whence), static_cast<GnomeVFSFileOffset>(offset));
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
FileSize Handle::tell() throw(exception)
#else
FileSize Handle::tell(std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GnomeVFSFileSize offset_return = 0;
  GnomeVFSResult result = gnome_vfs_tell(gobj(), &offset_return);
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED

  return static_cast<FileSize>(offset_return);
}

//static:
#ifdef GLIBMM_EXCEPTIONS_ENABLED
Glib::RefPtr<FileInfo> Handle::get_file_info(const Glib::ustring& text_uri, FileInfoOptions options) throw(exception)
#else
Glib::RefPtr<FileInfo> Handle::get_file_info(const Glib::ustring& text_uri, FileInfoOptions options, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GnomeVFSFileInfo* file_info = gnome_vfs_file_info_new();
  GnomeVFSResult result = gnome_vfs_get_file_info(text_uri.c_str(), file_info, static_cast<GnomeVFSFileInfoOptions>(options));
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
  return Glib::wrap(file_info);
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
Glib::RefPtr<FileInfo> Handle::get_file_info(FileInfoOptions options) const throw(exception)
#else
Glib::RefPtr<FileInfo> Handle::get_file_info(FileInfoOptions options, std::auto_ptr<Gnome::Vfs::exception>& error) const
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GnomeVFSFileInfo* file_info = gnome_vfs_file_info_new();
  GnomeVFSResult result = gnome_vfs_get_file_info_from_handle(const_cast<GnomeVFSHandle*>(gobj()), file_info, static_cast<GnomeVFSFileInfoOptions>(options));
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
  return Glib::wrap(file_info);
}

//static:
#ifdef GLIBMM_EXCEPTIONS_ENABLED
void Handle::truncate(const Glib::ustring& text_uri, FileSize length) throw(exception)
#else
void Handle::truncate(const Glib::ustring& text_uri, FileSize length, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GnomeVFSResult result = gnome_vfs_truncate(text_uri.c_str(), static_cast<GnomeVFSFileSize>(length));
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}

//static:
#ifdef GLIBMM_EXCEPTIONS_ENABLED
void Handle::truncate(const Glib::RefPtr<const Uri>& uri, FileSize length) throw(exception)
#else
void Handle::truncate(const Glib::RefPtr<const Uri>& uri, FileSize length, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GnomeVFSResult result = gnome_vfs_truncate_uri(const_cast<GnomeVFSURI*>(uri->gobj()), static_cast<GnomeVFSFileSize>(length));
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void Handle::truncate(FileSize length) throw(exception)
#else
void Handle::truncate(FileSize length, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GnomeVFSResult result = gnome_vfs_truncate_handle(gobj(), static_cast<GnomeVFSFileSize>(length));
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}

//static:
#ifdef GLIBMM_EXCEPTIONS_ENABLED
void Handle::make_directory(const Glib::ustring& text_uri, guint permissions) throw(exception)
#else
void Handle::make_directory(const Glib::ustring& text_uri, guint permissions, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GnomeVFSResult result = gnome_vfs_make_directory(text_uri.c_str(), permissions);
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}

//static:
#ifdef GLIBMM_EXCEPTIONS_ENABLED
void Handle::make_directory(const Glib::RefPtr<const Uri>& uri, guint permissions) throw(exception)
#else
void Handle::make_directory(const Glib::RefPtr<const Uri>& uri, guint permissions, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GnomeVFSResult result = gnome_vfs_make_directory_for_uri(const_cast<GnomeVFSURI*>(uri->gobj()), permissions);
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}

//static:
#ifdef GLIBMM_EXCEPTIONS_ENABLED
void Handle::remove_directory(const Glib::ustring& text_uri) throw(exception)
#else
void Handle::remove_directory(const Glib::ustring& text_uri, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GnomeVFSResult result = gnome_vfs_remove_directory(text_uri.c_str());
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}

//static:
#ifdef GLIBMM_EXCEPTIONS_ENABLED
void Handle::remove_directory(const Glib::RefPtr<const Uri>& uri) throw(exception)
#else
void Handle::remove_directory(const Glib::RefPtr<const Uri>& uri, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GnomeVFSResult result = gnome_vfs_remove_directory_from_uri(const_cast<GnomeVFSURI*>(uri->gobj()));
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}

//static:
#ifdef GLIBMM_EXCEPTIONS_ENABLED
void Handle::unlink(const Glib::ustring& text_uri) throw(exception)
#else
void Handle::unlink(const Glib::ustring& text_uri, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GnomeVFSResult result = gnome_vfs_unlink(text_uri.c_str());
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}

//static:
#ifdef GLIBMM_EXCEPTIONS_ENABLED
void Handle::unlink(const Glib::RefPtr<const Uri>& uri) throw(exception)
#else
void Handle::unlink(const Glib::RefPtr<const Uri>& uri, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GnomeVFSResult result = gnome_vfs_unlink_from_uri(const_cast<GnomeVFSURI*>(uri->gobj()));
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}

//static:
#ifdef GLIBMM_EXCEPTIONS_ENABLED
void Handle::move(const Glib::ustring& old_text_uri, const Glib::ustring& new_text_uri, bool force_replace) throw(exception)
#else
void Handle::move(const Glib::ustring& old_text_uri, const Glib::ustring& new_text_uri, bool force_replace, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GnomeVFSResult result = gnome_vfs_move(old_text_uri.c_str(), new_text_uri.c_str(), force_replace);
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}

//static:
#ifdef GLIBMM_EXCEPTIONS_ENABLED
void Handle::move(const Glib::RefPtr<const Uri>& old_uri, const Glib::RefPtr<const Uri>& new_uri, bool force_replace) throw(exception)
#else
void Handle::move(const Glib::RefPtr<const Uri>& old_uri, const Glib::RefPtr<const Uri>& new_uri, bool force_replace, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GnomeVFSResult result = gnome_vfs_move_uri(const_cast<GnomeVFSURI*>(old_uri->gobj()), const_cast<GnomeVFSURI*>(new_uri->gobj()), force_replace);
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}

//static:
#ifdef GLIBMM_EXCEPTIONS_ENABLED
bool Handle::check_same_fs(const Glib::ustring& source, const Glib::ustring& target) throw(exception)
#else
bool Handle::check_same_fs(const Glib::ustring& source, const Glib::ustring& target, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  gboolean bResult = FALSE;
  GnomeVFSResult result = gnome_vfs_check_same_fs(source.c_str(), target.c_str(), &bResult);
  return bResult;
}

//static:
#ifdef GLIBMM_EXCEPTIONS_ENABLED
bool Handle::check_same_fs(const Glib::RefPtr<const Uri>& source, const Glib::RefPtr<const Uri>& target) throw(exception)
#else
bool Handle::check_same_fs(const Glib::RefPtr<const Uri>& source, const Glib::RefPtr<const Uri>& target, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  gboolean bResult = FALSE;
  GnomeVFSResult result = gnome_vfs_check_same_fs_uris(const_cast<GnomeVFSURI*>(source->gobj()), const_cast<GnomeVFSURI*>(target->gobj()), &bResult);
  return bResult;
}

//static:
#ifdef GLIBMM_EXCEPTIONS_ENABLED
void Handle::set_file_info(const Glib::ustring& text_uri, const Glib::RefPtr<const FileInfo>& info, SetFileInfoMask mask) throw(exception)
#else
void Handle::set_file_info(const Glib::ustring& text_uri, const Glib::RefPtr<const FileInfo>& info, SetFileInfoMask mask, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GnomeVFSResult result = gnome_vfs_set_file_info(text_uri.c_str(), const_cast<GnomeVFSFileInfo*>(info->gobj()), static_cast<GnomeVFSSetFileInfoMask>(mask) );
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}

//static:
#ifdef GLIBMM_EXCEPTIONS_ENABLED
void Handle::set_file_info(const Glib::RefPtr<const Uri>& uri, const Glib::RefPtr<const FileInfo>& info, SetFileInfoMask mask) throw(exception)
#else
void Handle::set_file_info(const Glib::RefPtr<const Uri>& uri, const Glib::RefPtr<const FileInfo>& info, SetFileInfoMask mask, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GnomeVFSResult result = gnome_vfs_set_file_info_uri(const_cast<GnomeVFSURI*>(uri->gobj()), const_cast<GnomeVFSFileInfo*>(info->gobj()), static_cast<GnomeVFSSetFileInfoMask>(mask) );
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void Handle::file_control(const Glib::ustring& operation, gpointer operation_data) throw(exception)
#else
void Handle::file_control(const Glib::ustring& operation, gpointer operation_data, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif //GLIBMM_EXCEPTIONS_ENABLED
{
  GnomeVFSResult result = gnome_vfs_file_control(gobj(), operation.c_str(), operation_data);
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif //GLIBMM_EXCEPTIONS_ENABLED
}
  
} //namespace Vfs

} //namespace Gnome



