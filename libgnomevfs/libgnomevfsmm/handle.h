/* $Id$ */

/* Copyright 2003 gnome-vfsmm Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#ifndef _LIBGNOMEVFSMM_HANDLE_H
#define _LIBGNOMEVFSMM_HANDLE_H

#include <glibmm.h>

#include <libgnomevfs/gnome-vfs-ops.h>

#include <libgnomevfsmm/uri.h>
#include <libgnomevfsmm/exception.h>
#include <libgnomevfsmm/file-info.h>
#include <libgnomevfsmm/enums.h>
#include <libgnomevfsmm/types.h>


#ifndef DOXYGEN_SHOULD_SKIP_THIS
typedef struct GnomeVFSHandle GnomeVFSHandle;
#endif /* DOXYGEN_SHOULD_SKIP_THIS */

namespace Gnome
{

namespace Vfs
{

class Handle
{
public:
  Handle();
  virtual ~Handle();

#ifdef GLIBMM_EXCEPTIONS_ENABLED
  void open(const Glib::ustring& text_uri, OpenMode open_mode) throw(exception);
  void open(const Glib::RefPtr<const Uri>& uri, OpenMode open_mode) throw(exception);

  void create(const Glib::ustring& text_uri, OpenMode open_mode, bool exclusive, guint permissions) throw(exception);
  void create(const Glib::RefPtr<const Uri>& uri, OpenMode open_mode, bool exclusive, guint permissions) throw(exception);

  void close() throw(exception);

  FileSize read(gpointer buffer, FileSize bytes) throw(exception);

  FileSize write(gconstpointer buffer, FileSize bytes) throw(exception);

  void seek(SeekPosition whence, FileOffset offset) throw(exception);

  FileSize tell() throw(exception);

  static Glib::RefPtr<FileInfo> get_file_info(const Glib::ustring& text_uri, FileInfoOptions options = FILE_INFO_DEFAULT) throw(exception);

  Glib::RefPtr<FileInfo> get_file_info(FileInfoOptions options = FILE_INFO_DEFAULT) const throw(exception);


  static void truncate(const Glib::ustring& text_uri, FileSize length) throw(exception);
  static void truncate(const Glib::RefPtr<const Uri>& uri, FileSize length) throw(exception);
  void truncate(FileSize length) throw(exception);

  static void make_directory(const Glib::ustring& text_uri, guint permissions) throw(exception);
  static void make_directory(const Glib::RefPtr<const Uri>& uri, guint permissions) throw(exception);

  static void remove_directory(const Glib::ustring& text_uri) throw(exception);
  static void remove_directory(const Glib::RefPtr<const Uri>& uri) throw(exception);

  static void unlink(const Glib::ustring& text_uri) throw(exception);
  static void unlink(const Glib::RefPtr<const Uri>& uri) throw(exception);

  static void move(const Glib::ustring& old_text_uri, const Glib::ustring& new_text_uri, bool force_replace) throw(exception);
  static void move(const Glib::RefPtr<const Uri>& old_uri, const Glib::RefPtr<const Uri>& new_uri, bool force_replace) throw(exception);

  static bool check_same_fs(const Glib::ustring& source, const Glib::ustring& target) throw(exception);
  static bool check_same_fs(const Glib::RefPtr<const Uri>& source, const Glib::RefPtr<const Uri>& target) throw(exception);

  bool uri_exists(const Glib::RefPtr<const Uri>& uri) throw(exception);

  static void set_file_info(const Glib::ustring& text_uri, const Glib::RefPtr<const FileInfo>& info, SetFileInfoMask mask) throw(exception);
  static void set_file_info(const Glib::RefPtr<const Uri>& uri, const Glib::RefPtr<const FileInfo>& info, SetFileInfoMask mask) throw(exception);

  void file_control(const Glib::ustring& operation, gpointer operation_data) throw(exception);
#else
  void open(const Glib::ustring& text_uri, OpenMode open_mode, std::auto_ptr<Gnome::Vfs::exception>& error);
  void open(const Glib::RefPtr<const Uri>& uri, OpenMode open_mode, std::auto_ptr<Gnome::Vfs::exception>& error);

  void create(const Glib::ustring& text_uri, OpenMode open_mode, bool exclusive, guint permissions, std::auto_ptr<Gnome::Vfs::exception>& error);
  void create(const Glib::RefPtr<const Uri>& uri, OpenMode open_mode, bool exclusive, guint permissions, std::auto_ptr<Gnome::Vfs::exception>& error);

  void close(std::auto_ptr<Gnome::Vfs::exception>& error);

  FileSize read(gpointer buffer, FileSize bytes, std::auto_ptr<Gnome::Vfs::exception>& error);

  FileSize write(gconstpointer buffer, FileSize bytes, std::auto_ptr<Gnome::Vfs::exception>& error);

  void seek(SeekPosition whence, FileOffset offset, std::auto_ptr<Gnome::Vfs::exception>& error);

  FileSize tell(std::auto_ptr<Gnome::Vfs::exception>& error);

  static Glib::RefPtr<FileInfo> get_file_info(const Glib::ustring& text_uri, FileInfoOptions options, std::auto_ptr<Gnome::Vfs::exception>& error);

  Glib::RefPtr<FileInfo> get_file_info(FileInfoOptions options, std::auto_ptr<Gnome::Vfs::exception>& error) const;


  static void truncate(const Glib::ustring& text_uri, FileSize length, std::auto_ptr<Gnome::Vfs::exception>& error);
  static void truncate(const Glib::RefPtr<const Uri>& uri, FileSize length, std::auto_ptr<Gnome::Vfs::exception>& error);
  void truncate(FileSize length, std::auto_ptr<Gnome::Vfs::exception>& error);

  static void make_directory(const Glib::ustring& text_uri, guint permissions, std::auto_ptr<Gnome::Vfs::exception>& error);
  static void make_directory(const Glib::RefPtr<const Uri>& uri, guint permissions, std::auto_ptr<Gnome::Vfs::exception>& error);

  static void remove_directory(const Glib::ustring& text_uri, std::auto_ptr<Gnome::Vfs::exception>& error);
  static void remove_directory(const Glib::RefPtr<const Uri>& uri, std::auto_ptr<Gnome::Vfs::exception>& error);

  static void unlink(const Glib::ustring& text_uri, std::auto_ptr<Gnome::Vfs::exception>& error);
  static void unlink(const Glib::RefPtr<const Uri>& uri, std::auto_ptr<Gnome::Vfs::exception>& error);

  static void move(const Glib::ustring& old_text_uri, const Glib::ustring& new_text_uri, bool force_replace, std::auto_ptr<Gnome::Vfs::exception>& error);
  static void move(const Glib::RefPtr<const Uri>& old_uri, const Glib::RefPtr<const Uri>& new_uri, bool force_replace, std::auto_ptr<Gnome::Vfs::exception>& error);

  static bool check_same_fs(const Glib::ustring& source, const Glib::ustring& target, std::auto_ptr<Gnome::Vfs::exception>& error);
  static bool check_same_fs(const Glib::RefPtr<const Uri>& source, const Glib::RefPtr<const Uri>& target, std::auto_ptr<Gnome::Vfs::exception>& error);

  bool uri_exists(const Glib::RefPtr<const Uri>& uri, std::auto_ptr<Gnome::Vfs::exception>& error);

  static void set_file_info(const Glib::ustring& text_uri, const Glib::RefPtr<const FileInfo>& info, SetFileInfoMask mask, std::auto_ptr<Gnome::Vfs::exception>& error);
  static void set_file_info(const Glib::RefPtr<const Uri>& uri, const Glib::RefPtr<const FileInfo>& info, SetFileInfoMask mask, std::auto_ptr<Gnome::Vfs::exception>& error);

  void file_control(const Glib::ustring& operation, gpointer operation_data, std::auto_ptr<Gnome::Vfs::exception>& error);
#endif //GLIBMM_EXCEPTIONS_ENABLED


  GnomeVFSHandle* gobj();
  const GnomeVFSHandle* gobj() const;  


protected:
 
  GnomeVFSHandle* gobj_;
};

} // namespace Vfs
} // namespace Gnome




#endif /* _LIBGNOMEVFSMM_HANDLE_H */

