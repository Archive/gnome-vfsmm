/* Copyright 2003 gnome-vfsmm Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef _LIBGNOMEVFSMM_TYPES_H
#define _LIBGNOMEVFSMM_TYPES_H

#include <libgnomevfs/gnome-vfs-file-size.h>

namespace Gnome
{

namespace Vfs
{

typedef GnomeVFSFileSize FileSize;
typedef GnomeVFSFileOffset FileOffset;

} // namespace Vfs

} // namespace Gnome

#endif
