/* Copyright 2003, 2006 gnome-vfsmm Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */


#ifndef _LIBGNOMEVFSMM_TRANSFER_H
#define _LIBGNOMEVFSMM_TRANSFER_H

#include <glibmm.h>

#include <libgnomevfsmm/enums.h>
#include <libgnomevfsmm/exception.h>
#include <libgnomevfsmm/uri.h>
#include <libgnomevfsmm/transfer-progress.h>

#include <libgnomevfs/gnome-vfs-xfer.h> //For GnomeVFSXferProgressInfo

namespace Gnome
{

namespace Vfs
{

/** @deprecated Use the Transfer2 namespace instead.
 */
namespace Transfer
{

typedef Glib::ListHandle<Glib::ustring> ListHandleStrings;
typedef Glib::ListHandle< Glib::RefPtr<const Uri> > ListHandleUris;

/// For instance, int on_transfer_progress(GnomeVFSXferProgressInfo* info);
typedef sigc::slot<bool, const ProgressInfo&> SlotProgress;


#ifdef GLIBMM_EXCEPTIONS_ENABLED
void transfer_list(const Glib::StringArrayHandle& source_uri_list, const Glib::StringArrayHandle& target_uri_list,
      TransferOptions options,
      ErrorMode error_mode,
      OverwriteMode overwrite_mode,
      const SlotProgress& slot);
#else
void transfer_list(const Glib::StringArrayHandle& source_uri_list, const Glib::StringArrayHandle& target_uri_list,
      TransferOptions options,
      ErrorMode error_mode,
      OverwriteMode overwrite_mode,
      const SlotProgress& slot,
      std::auto_ptr<Gnome::Vfs::exception> error);
#endif //GLIBMM_EXCEPTIONS_ENABLED
      
/**
 * @source_uri_list: A List of uris  (ie file://)
 * @target_uri_list: A List of uris
 * @options: These are options you wish to set for the transfer. For
 * instance by setting the xfer behavior you can either make a copy or a move.
 * @error_mode:  Decide how to behave if the xfer is interrupted.  For instance
 * you could set your application to return an error code in case of an
 * interuption.
 * @overwrite_mode: How to react if a file your copying is being overwritten.
 * @progress_callback:  This is used to monitor the progress of a transfer.
 * Common use would be to check to see if the transfer is asking for permission
 * to overwrite a file.
 *
 * This function will transfer multiple files to multiple targets.  Given a
 * a source uri(s) and a destination uri(s).   There are a list of options that
 * your application can use to control how the transfer is done.
 *
 */
#ifdef GLIBMM_EXCEPTIONS_ENABLED
void transfer_list_uris(const ListHandleUris& source_uri_list, const ListHandleUris& target_uri_list,
      TransferOptions options = XFER_DEFAULT,
      ErrorMode error_mode = XFER_ERROR_MODE_ABORT,
      OverwriteMode overwrite_mode = XFER_OVERWRITE_MODE_ABORT,
      const SlotProgress& progress_callback = SlotProgress());
#else
void transfer_list_uris(const ListHandleUris& source_uri_list, const ListHandleUris& target_uri_list,
      TransferOptions options,
      ErrorMode error_mode,
      OverwriteMode overwrite_mode,
      const SlotProgress& progress_callback,
      std::auto_ptr<Gnome::Vfs::exception>& error);
#endif //GLIBMM_EXCEPTIONS_ENABLED

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void transfer(const Glib::ustring& source_uri, const Glib::ustring& target_uri,
      TransferOptions options = XFER_DEFAULT,
      ErrorMode error_mode = XFER_ERROR_MODE_ABORT,
      OverwriteMode overwrite_mode = XFER_OVERWRITE_MODE_ABORT,
      const SlotProgress& progress_callback = SlotProgress());
#else
void transfer(const Glib::ustring& source_uri, const Glib::ustring& target_uri,
      TransferOptions options,
      ErrorMode error_mode,
      OverwriteMode overwrite_mode,
      const SlotProgress& progress_callback,
      std::auto_ptr<Gnome::Vfs::exception>& error);
#endif //GLIBMM_EXCEPTIONS_ENABLED
      
#ifdef GLIBMM_EXCEPTIONS_ENABLED
void transfer(const Glib::RefPtr<const Uri>& source_uri, const Glib::RefPtr<const Uri>& target_uri,
      TransferOptions options = XFER_DEFAULT,
      ErrorMode error_mode = XFER_ERROR_MODE_ABORT,
      OverwriteMode overwrite_mode = XFER_OVERWRITE_MODE_ABORT,
      const SlotProgress& progress_callback = SlotProgress());
#else
void transfer(const Glib::RefPtr<const Uri>& source_uri, const Glib::RefPtr<const Uri>& target_uri,
      TransferOptions options,
      ErrorMode error_mode,
      OverwriteMode overwrite_mode,
      const SlotProgress& progress_callback,
      std::auto_ptr<Gnome::Vfs::exception>& error);
#endif //GLIBMM_EXCEPTIONS_ENABLED

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void remove(const Glib::ustring& source_uri,
      TransferOptions options = XFER_DEFAULT,
      ErrorMode error_mode = XFER_ERROR_MODE_ABORT,
      const SlotProgress& progress_callback = SlotProgress());
#else
void remove(const Glib::ustring& source_uri,
      TransferOptions options,
      ErrorMode error_mode,
      const SlotProgress& progress_callback,
      std::auto_ptr<Gnome::Vfs::exception>& error);
#endif //GLIBMM_EXCEPTIONS_ENABLED
      
#ifdef GLIBMM_EXCEPTIONS_ENABLED
void remove(const Glib::RefPtr<const Uri>& source_uri,
      TransferOptions options = XFER_DEFAULT,
      ErrorMode error_mode = XFER_ERROR_MODE_ABORT,
      const SlotProgress& progress_callback = SlotProgress());
#else
void remove(const Glib::RefPtr<const Uri>& source_uri,
      TransferOptions options,
      ErrorMode error_mode,
      const SlotProgress& progress_callback,
      std::auto_ptr<Gnome::Vfs::exception>& error);
#endif //GLIBMM_EXCEPTIONS_ENABLED

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void remove_list(const Glib::StringArrayHandle& source_uri_list,
      TransferOptions options,
      ErrorMode error_mode,
      const SlotProgress& slot);
#else
void remove_list(const Glib::StringArrayHandle& source_uri_list,
      TransferOptions options,
      ErrorMode error_mode,
      const SlotProgress& slot,
      std::auto_ptr<Gnome::Vfs::exception>& error);
#endif //GLIBMM_EXCEPTIONS_ENABLED

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void remove_list_uris(const ListHandleUris& source_uri_list,
      TransferOptions options = XFER_DEFAULT,
      ErrorMode error_mode = XFER_ERROR_MODE_ABORT,
      const SlotProgress& progress_callback = SlotProgress());
#else
void remove_list_uris(const ListHandleUris& source_uri_list,
      TransferOptions options,
      ErrorMode error_mode,
      const SlotProgress& progress_callbacki,
      std::auto_ptr<Gnome::Vfs::exception>& error);
#endif //GLIBMM_EXCEPTIONS_ENABLED

} // namespace Transfer
} // namespace Vfs
} // namespace Gnome

#endif /* _LIBGNOMEVFSMM_TRANSFER_H */
