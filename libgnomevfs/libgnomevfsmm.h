/* $Id$ */
/* libgnomevfsmm - a C++ wrapper for libgda
 *
 * Copyright 1999-2003 Free Software Foundation
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#ifndef LIBGNOMEVFSMM_H
#define LIBGNOMEVFSMM_H

/* libgnomevfsmm version.  */
extern const int libgnomevfsmm_major_version;
extern const int libgnomevfsmm_minor_version;
extern const int libgnomevfsmm_micro_version;

#include <glibmm.h>

#include <libgnomevfsmm/init.h>
#include <libgnomevfsmm/application-registry.h>
#include <libgnomevfsmm/async.h>
#include <libgnomevfsmm/async-2.h>
#include <libgnomevfsmm/async-handle.h>
#include <libgnomevfsmm/async-handle-2.h>
#include <libgnomevfsmm/enums.h>
#include <libgnomevfsmm/types.h>
#include <libgnomevfsmm/directory-handle.h>
#include <libgnomevfsmm/handle.h>
#include <libgnomevfsmm/file-info.h>
#include <libgnomevfsmm/mime-application.h>
#include <libgnomevfsmm/mime-action.h>
#include <libgnomevfsmm/mime-handlers.h>
#include <libgnomevfsmm/mime-monitor.h>
#include <libgnomevfsmm/uri.h>
#include <libgnomevfsmm/monitor-handle.h>
#include <libgnomevfsmm/transfer-progress.h>
#include <libgnomevfsmm/transfer-progress-2.h>
#include <libgnomevfsmm/transfer.h>
#include <libgnomevfsmm/transfer-2.h>
#include <libgnomevfsmm/drive.h>
#include <libgnomevfsmm/volume.h>
#include <libgnomevfsmm/volume-monitor.h>
#include <libgnomevfsmm/utils.h>
#include <libgnomevfsmm/dns-sd.h>

#endif /* #ifndef LIBGNOMEVFSMM_H */
