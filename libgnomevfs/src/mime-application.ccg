/* Copyright 2003 gnome-vfsmm Development Team
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <libgnomevfsmm/private.h>

namespace Gnome
{

namespace Vfs
{

MimeApplication::MimeApplication(const Glib::ustring& id)
: gobject_( gnome_vfs_mime_application_new_from_id(id.c_str()) )
{  
}

MimeApplication::MimeApplication(const Glib::ustring& id, const Glib::ustring& name,
      const Glib::ustring& command, MimeApplicationArgumentType argument_type, 
      ListHandleStrings supported_uri_schemes, bool multiple_files, bool requires_terminal)
{
  gobject_ = g_new0(GnomeVFSMimeApplication, 1);
  
  gobject_->id = g_strdup(id.c_str());
  gobject_->name = g_strdup(name.c_str());
  gobject_->command = g_strdup(command.c_str());
  gobject_->expects_uris = static_cast<GnomeVFSMimeApplicationArgumentType>(argument_type);
  gobject_->supported_uri_schemes = supported_uri_schemes.data();
  gobject_->can_open_multiple_files = multiple_files;
  gobject_->requires_terminal = requires_terminal;
}

MimeApplication::operator bool() const
{
  return ( gobj() != 0 );
}

bool MimeApplication::can_open_multiple_files() const
{
  return gobj()->can_open_multiple_files;
}

bool MimeApplication::requires_terminal() const
{
  return gobj()->requires_terminal;
}

void MimeApplication::set_open_multiple_files(bool value)
{
  gobj()->can_open_multiple_files = value;
}

void MimeApplication::set_requires_terminal(bool value)
{
  gobj()->requires_terminal = value;
}

bool MimeApplication::exists_in_registry() const
{
  return gnome_vfs_application_registry_exists(gobj()->id);
}

void MimeApplication::remove_from_registry()
{
  gnome_vfs_application_registry_remove_application(gobj()->id);
}

ListHandleStrings MimeApplication::get_keys() const
{
  GList* pList = gnome_vfs_application_registry_get_keys(gobj()->id);
  return ListHandleStrings(pList, Glib::OWNERSHIP_SHALLOW);
}

ListHandleStrings MimeApplication::get_mime_types() const
{
  GList* pList = gnome_vfs_application_registry_get_mime_types(gobj()->id);
  return ListHandleStrings(pList, Glib::OWNERSHIP_SHALLOW);
}

void MimeApplication::clear_mime_types()
{
  gnome_vfs_application_registry_clear_mime_types(gobj()->id);
}

bool MimeApplication::supports_mime_type(const Glib::ustring& mime_type) const
{
  return gnome_vfs_application_registry_supports_mime_type(gobj()->id, mime_type.c_str());
}

bool MimeApplication::supports_uri_scheme(const Glib::ustring& uri_scheme) const
{
  return gnome_vfs_application_registry_supports_uri_scheme(gobj()->id, uri_scheme.c_str());
}

Glib::ustring MimeApplication::get_value(const Glib::ustring& key) const
{
  return Glib::convert_const_gchar_ptr_to_ustring(gnome_vfs_application_registry_peek_value(gobj()->id, key.c_str()));
}

bool MimeApplication::get_value_bool(const Glib::ustring& key, bool& got_key) const
{
  gboolean c_got_key = FALSE; 
  bool result = gnome_vfs_application_registry_get_bool_value(gobj()->id, key.c_str(), &c_got_key);
  got_key = c_got_key;
  return result;
}

bool MimeApplication::get_value_bool(const Glib::ustring& key) const
{
  gboolean c_got_key = FALSE;
  return gnome_vfs_application_registry_get_bool_value(gobj()->id, key.c_str(), &c_got_key);
}

void MimeApplication::set_value(const Glib::ustring& key, const Glib::ustring& value)
{
  gnome_vfs_application_registry_set_value(gobj()->id, key.c_str(), value.c_str());
}

void MimeApplication::set_value(const Glib::ustring& key, bool value)
{
  gnome_vfs_application_registry_set_bool_value(gobj()->id, key.c_str(), value);
}

void MimeApplication::unset_key(const Glib::ustring& key)
{
  gnome_vfs_application_registry_unset_key(gobj()->id, key.c_str());
}

void MimeApplication::add_mime_type(const Glib::ustring& mime_type)
{
  gnome_vfs_application_registry_add_mime_type(gobj()->id, mime_type.c_str());
}

void MimeApplication::remove_mime_type(const Glib::ustring& mime_type)
{
  gnome_vfs_application_registry_remove_mime_type(gobj()->id, mime_type.c_str());
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void MimeApplication::set_default(const Glib::ustring& mime_type) throw(exception)
#else
void MimeApplication::set_default(const Glib::ustring& mime_type, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif
{
  GnomeVFSResult result = gnome_vfs_mime_set_default_application(mime_type.c_str(), gobj()->id);
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void MimeApplication::add_to_short_list(const Glib::ustring& mime_type) throw(exception)
#else
void MimeApplication::add_to_short_list(const Glib::ustring& mime_type, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif
{
  GnomeVFSResult result = gnome_vfs_mime_add_application_to_short_list(mime_type.c_str(), gobj()->id);
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void MimeApplication::remove_from_short_list(const Glib::ustring& mime_type) throw(exception)
#else
void MimeApplication::remove_from_short_list(const Glib::ustring& mime_type, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif
{
  GnomeVFSResult result = gnome_vfs_mime_remove_application_from_short_list(mime_type.c_str(), gobj()->id);
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void MimeApplication::launch(const Glib::ListHandle<Glib::ustring>& uris) throw(exception)
#else
void MimeApplication::launch(const Glib::ListHandle<Glib::ustring>& uris, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif
{
  GnomeVFSResult result = gnome_vfs_mime_application_launch(gobj(), uris.data());
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif
}

#ifdef GLIBMM_EXCEPTIONS_ENABLED
void MimeApplication::launch(const Glib::ListHandle<Glib::ustring>& uris, const Glib::ustring& envp) throw(exception)
#else
void MimeApplication::launch(const Glib::ListHandle<Glib::ustring>& uris, const Glib::ustring& envp, std::auto_ptr<Gnome::Vfs::exception>& error)
#endif
{
  const char* temp_str = envp.c_str();
  GnomeVFSResult result = gnome_vfs_mime_application_launch_with_env(gobj(), uris.data(), const_cast<char**>(&temp_str));
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  handle_result(result);
  #else
  handle_result(result, error);
  #endif
}

} //namespace Vfs

} //namespace Gnome
