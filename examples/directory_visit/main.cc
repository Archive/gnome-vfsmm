/* main.cc
 *
 * Copyright (C) 2003 gnome-vfsmm Development Team
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <glibmm.h>
#include <libgnomevfsmm.h>
#include <iostream>
#include <vector>

// use this to change a Gnome::Vfs::FileType to a ustring
Glib::ustring type_to_string (Gnome::Vfs::FileType type)
{
  // this is used to make sure we have one entry point and one
  // exit point... I learned this somewhere in school :)
  Glib::ustring returnType;
  
  switch (type) {
  case Gnome::Vfs::FILE_TYPE_UNKNOWN_TYPE:
    returnType = "Unknown";
    break;
  case Gnome::Vfs::FILE_TYPE_REGULAR:
    returnType = "Regular";
    break;
  case Gnome::Vfs::FILE_TYPE_DIRECTORY:
    returnType = "Directory";
    break;
  case Gnome::Vfs::FILE_TYPE_SYMBOLIC_LINK:
    returnType = "Symbolic Link";
    break;
  case Gnome::Vfs::FILE_TYPE_FIFO:
    returnType = "FIFO";
    break;
  case Gnome::Vfs::FILE_TYPE_SOCKET:
    returnType = "Socket";
    break;
  case Gnome::Vfs::FILE_TYPE_CHARACTER_DEVICE:
    returnType = "Character device";
    break;
  case Gnome::Vfs::FILE_TYPE_BLOCK_DEVICE:
    returnType = "Block device";
    break;
  default:
    returnType = "???";
    break;
  }

  return returnType;
}

// print our error (if there is one)
int print_error (const Gnome::Vfs::exception& ex, const std::string& uri_string)
{
  std::cout << "Error occurred opening location " << uri_string << std:: endl
            << "  " << ex.what() << std::endl;

  return 1;
}

// print the info we get from the file info in our callback
void print_file(const Glib::ustring name, const Gnome::Vfs::FileType type, const Glib::ustring mime_type, const Gnome::Vfs::FileSize size, const Gnome::Vfs::FilePermissions perms)
{
  std::cout << "  File: " << name << (type == Gnome::Vfs::FILE_TYPE_SYMBOLIC_LINK? "[link]" : "") <<
    " (" << type_to_string(type) << ", " << mime_type <<
    "), size: " << size << ", mode: " << perms << std::endl;
}

// here's our fun callback!!  see Gnome::Vfs::SlotVisit to see where I got the types
bool on_visit(const Glib::ustring& rel_path, const Glib::RefPtr<const Gnome::Vfs::FileInfo>& info, bool recursing_will_loop, bool& recurse)
{
  std::cout << "visit_callback -- relpath: " << rel_path << std::endl;
  print_file(info->get_name(), info->get_type(), info->get_mime_type(), info->get_size(), info->get_permissions());
  
  // we don't want to recurse into the directories
  recurse = false;

  // we want to keep going after each file until there are no more....
  return true;
}

int main(int argc, char** argv)
{
  //Get the command-line arguments
  std::string input_uri_string, output_uri_string;

  if( argc > 1 )
  {
    input_uri_string = argv[1];
  }
  else
  {
    std::cout << "Call this with a URLs, such as" << std::endl
              << "./example file:///home/bryan/" << std::endl;
    return 0;
  }
  
  // for testing:
  //input_uri_string = "file:///home/bryan";
  
  //Initialize gnome-vfsmm:
  Gnome::Vfs::init();

#ifdef GLIBMM_EXCEPTIONS_ENABLED
  try
  {
    // Call on_visit() for each file.
    // The options specify that we want to visit the files at input_uri_string,
    // get the mime type the fast way and protect against loops.
    Gnome::Vfs::DirectoryHandle::visit(input_uri_string, Gnome::Vfs::FILE_INFO_GET_MIME_TYPE |
                                             Gnome::Vfs::FILE_INFO_FORCE_FAST_MIME_TYPE,
                                             Gnome::Vfs::DIRECTORY_VISIT_LOOPCHECK,
                                             sigc::ptr_fun(on_visit));
  }
  catch(const Gnome::Vfs::exception& ex)
  {
    // if there are any errors... use this to print them out
    return print_error(ex, input_uri_string);
  }
#else
  std::auto_ptr<Gnome::Vfs::exception> error;
  // Call on_visit() for each file.
  // The options specify that we want to visit the files at input_uri_string,
  // get the mime type the fast way and protect against loops.
  Gnome::Vfs::DirectoryHandle::visit(input_uri_string, Gnome::Vfs::FILE_INFO_GET_MIME_TYPE |
                                           Gnome::Vfs::FILE_INFO_FORCE_FAST_MIME_TYPE,
                                           Gnome::Vfs::DIRECTORY_VISIT_LOOPCHECK,
                                           sigc::ptr_fun(on_visit),
                                           error);

  if(error.get())
  {
    // if there are any errors... use this to print them out
    return print_error(*error.get(), input_uri_string);
  }
#endif //GLIBMM_EXCEPTIONS_ENABLED

  return 0;
}
