/* main.cc
 *
 * Example using async API.
 *
 * Copyright (C) 2007 gnome-vfsmm Development Team
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <cassert>
#include <iostream>
#include <list>
#include <memory>
#include <string>

#include <glibmm.h>
#include <libgnomevfsmm.h>

void print_vfs_error(const Gnome::Vfs::exception& ex)
{
  std::cout << "Exception occurred. Reason: " << ex.what() << std::endl;
}

class Application
{
public:
  explicit Application();
  ~Application();

  void run();
  void quit();

  void do_open_read(const std::string& path);
  void do_transfer(const std::string& src, const std::string& dest);

protected:
  void on_open(const Gnome::Vfs::Async2::Handle& handle, Gnome::Vfs::Result result);

  void on_read(const Gnome::Vfs::Async2::Handle& handle, Gnome::Vfs::Result result, gpointer buffer, Gnome::Vfs::FileSize bytes_requested, Gnome::Vfs::FileSize bytes_read);

  void on_close(const Gnome::Vfs::Async2::Handle& handle, Gnome::Vfs::Result result);
  
  int on_async_transfer_progress(const Gnome::Vfs::Async2::Handle& handle, Gnome::Vfs::Transfer2::ProgressInfo& progress_info);
  gint on_sync_transfer_progress(const Gnome::Vfs::Transfer2::ProgressInfo& progress_info);

private:
  Gnome::Vfs::Async2::Handle handle_;
  Glib::RefPtr<Gnome::Vfs::Uri> input_uri_;
  Glib::RefPtr<Glib::MainLoop> main_loop_;
};

Application::Application()
{
  std::cout << "Initializing gnome-vfs..." << std::endl;
  Gnome::Vfs::init();

  main_loop_ = Glib::MainLoop::create();
}

Application::~Application()
{
}

void Application::quit()
{
  if (main_loop_->is_running())
    main_loop_->quit();
}

void Application::on_close(const Gnome::Vfs::Async2::Handle& handle, Gnome::Vfs::Result result)
{
  std::cout << "Closing with result: " << gnome_vfs_result_to_string(static_cast<GnomeVFSResult>(result)) << std::endl;
  quit();
}

void Application::on_read(const Gnome::Vfs::Async2::Handle& handle, Gnome::Vfs::Result result, gpointer buffer, Gnome::Vfs::FileSize bytes_requested, Gnome::Vfs::FileSize bytes_read)
{
  std::cout << "Read " << bytes_read << " out of " << bytes_requested << " requested." << std::endl;

  //Uncomment if you want to see the buffer printed on the screen:
  //std::cout << static_cast<char*>(buffer);

  delete[] static_cast<char*>(buffer);

  std::cout << "Closing file." << std::endl;
  const_cast<Gnome::Vfs::Async2::Handle&>(handle).close(sigc::mem_fun(*this, &Application::on_close));
}

void Application::on_open(const Gnome::Vfs::Async2::Handle& handle, Gnome::Vfs::Result result)
{
  if (result == Gnome::Vfs::OK)
  {
    std::cout << "Open OK." << std::endl;

    Glib::RefPtr<Gnome::Vfs::FileInfo> file_info;
    Gnome::Vfs::FileSize file_size;
    gchar* buffer;
#ifdef GLIBMM_EXCEPTIONS_ENABLED
    try
    {
      file_info = input_uri_->get_file_info(Gnome::Vfs::FILE_INFO_DEFAULT);
      file_size = file_info->get_size();

      //TODO: It seems that it's not possible to read chunks of data. Ask gnome-vfs developers.
      buffer = new gchar[file_size];
      const_cast<Gnome::Vfs::Async2::Handle&>(handle).read(buffer, file_size, sigc::mem_fun(*this, &Application::on_read));
    }
    catch (const Gnome::Vfs::exception& ex)
    {
      print_vfs_error(ex);
      quit();
    }
#else
    std::auto_ptr<Gnome::Vfs::exception> error;

    file_info = input_uri_->get_file_info(Gnome::Vfs::FILE_INFO_DEFAULT, error);
    if (! error.get())
    {
      file_size = file_info->get_size();

      //TODO: It seems that it's not possible to read chunks of data. Ask gnome-vfs developers.
      buffer = new gchar[file_size];
      const_cast<Gnome::Vfs::Async2::Handle&>(handle).read(buffer, file_size, sigc::mem_fun(*this, &Application::on_read));
    }
    else
    {
      print_vfs_error(*error);
      quit();
    }
#endif // GLIBMM_EXCEPTIONS_ENABLED
  }
  else
  {
    std::cerr << "Open failed: " << gnome_vfs_result_to_string(static_cast<GnomeVFSResult>(result)) << std::endl;
    quit();
  }
}

void Application::do_open_read(const std::string& path)
{
  Glib::RefPtr<Gnome::Vfs::Uri> src = Gnome::Vfs::Uri::create(Gnome::Vfs::Uri::make_from_shell_arg(path));

  //Keep URI as a member so that we can later use it to determine the size of a buffer to allocate for reading.
  input_uri_ = src;

  std::cout << "Starting open() for " << input_uri_->get_path() << std::endl;

  handle_.open(input_uri_, Gnome::Vfs::OPEN_READ, Gnome::Vfs::Async2::Handle::PRIORITY_MIN, sigc::mem_fun(*this, &Application::on_open));
}

int Application::on_async_transfer_progress(const Gnome::Vfs::Async2::Handle& handle, Gnome::Vfs::Transfer2::ProgressInfo& info)
{
  if (info)
  {
    switch (info.get_status())
    {
      case Gnome::Vfs::XFER_PROGRESS_STATUS_VFSERROR:
        std::cout << "VFS Error: " << gnome_vfs_result_to_string(static_cast<GnomeVFSResult>(info.get_vfs_status())) << std::endl;
        std::cout << "Skipping " << info.get_source_name() << std::endl;
        // This is a matter of choice (see ErrorAction enum):
        return Gnome::Vfs::XFER_ERROR_ACTION_SKIP;

        //Note that we don't handle XFER_PROGRESS_STATUS_OVERWRITE, because in transfer()
        //we specified that we want to overwrite every time.

      case Gnome::Vfs::XFER_PROGRESS_STATUS_OK:
      {
        switch (info.get_phase())
        {
          case Gnome::Vfs::XFER_PHASE_INITIAL:
            std::cout << "  Initial transfer phase." << std::endl;
            break;
          case Gnome::Vfs::XFER_CHECKING_DESTINATION:
            std::cout << "  Checking destination." << std::endl;
            break;
          case Gnome::Vfs::XFER_PHASE_COLLECTING:
            std::cout << "  Collecting file list." << std::endl;
            break;
          case Gnome::Vfs::XFER_PHASE_READYTOGO:
            std::cout << "  Ready to go!" << std::endl;
            break;
          case Gnome::Vfs::XFER_PHASE_OPENSOURCE:
            std::cout << "  Opening source." << std::endl;
            break;
          case Gnome::Vfs::XFER_PHASE_OPENTARGET:
            std::cout << "  Opening target." << std::endl;
            break;
          case Gnome::Vfs::XFER_PHASE_COPYING:
            std::cout << "  Copying '" << info.get_source_name()
                      << "' to '" << info.get_target_name()
                      << "' (file " << info.get_file_index()
                      << "/" << info.get_total_files()
                      << ", byte " << info.get_bytes_copied()
                      << "/" << info.get_file_size()
                      << " in file, " << info.get_total_bytes_copied()
                      << "/" << info.get_total_bytes() << std::endl;
            break;
          case Gnome::Vfs::XFER_PHASE_MOVING:
            std::cout << "  Moving '" << info.get_source_name()
                      << "' to '" << info.get_target_name()
                      << "' (file " << info.get_file_index()
                      << "/" << info.get_total_files()
                      << ", byte " << info.get_bytes_copied()
                      << "/" << info.get_file_size()
                      << " in file, " << info.get_total_bytes_copied()
                      << "/" << info.get_total_bytes() << std::endl;
            break;
          case Gnome::Vfs::XFER_PHASE_READSOURCE:
            std::cout << "  Reading source." << std::endl;
            break;
          case Gnome::Vfs::XFER_PHASE_CLOSESOURCE:
            std::cout << "  Closing source." << std::endl;
            break;
          case Gnome::Vfs::XFER_PHASE_CLOSETARGET:
            std::cout << "  Closing target." << std::endl;
            break;
          case Gnome::Vfs::XFER_PHASE_DELETESOURCE:
            std::cout << "  Deleting source." << std::endl;
            break;
          case Gnome::Vfs::XFER_PHASE_FILECOMPLETED:
            std::cout << "  Done with '" << info.get_source_name()
                      << "' -> '" << info.get_target_name() << std::endl;
            break;
          case Gnome::Vfs::XFER_PHASE_COMPLETED:
            std::cout << "  Completed. Exiting..." << std::endl;

            //Now we will...
            quit();

            break;
          default:
            std::cout << "  Unexpected phase " << info.get_phase()
                      << std::endl;
        }
      }
      case Gnome::Vfs::XFER_PROGRESS_STATUS_DUPLICATE:
        break;
    }
  }

  return true;
}

gint Application::on_sync_transfer_progress(const Gnome::Vfs::Transfer2::ProgressInfo& /* progress_info */)
{
  //Do nothing, we're not interested in this signal - see the docs for when it should be used.
  return true;
}

void Application::do_transfer(const std::string& src, const std::string& dest)
{
  Glib::RefPtr<Gnome::Vfs::Uri> src_uri = Gnome::Vfs::Uri::create(Gnome::Vfs::Uri::make_from_shell_arg(src));
  Glib::RefPtr<Gnome::Vfs::Uri> dest_uri = Gnome::Vfs::Uri::create(Gnome::Vfs::Uri::make_from_shell_arg(dest));

  Glib::RefPtr<Gnome::Vfs::FileInfo> src_info;

  std::list< Glib::RefPtr<Gnome::Vfs::Uri> > src_uri_list;
  std::list< Glib::RefPtr<Gnome::Vfs::Uri> > dest_uri_list;

#ifdef GLIBMM_EXCEPTIONS_ENABLED
  try
  {
    src_info = src_uri->get_file_info();
    
    if (src_info->get_type() == Gnome::Vfs::FILE_TYPE_DIRECTORY)
    {
      //Build the destination URI:
      dest_uri = dest_uri->append_string(src_uri->extract_short_path_name());
    }

    src_uri_list.push_back(src_uri);
    dest_uri_list.push_back(dest_uri);

    std::cout << "Transfering file(s)." << std::endl;

    handle_.transfer(src_uri_list, dest_uri_list, Gnome::Vfs::XFER_RECURSIVE, Gnome::Vfs::XFER_ERROR_MODE_QUERY, Gnome::Vfs::XFER_OVERWRITE_MODE_REPLACE, Gnome::Vfs::Async2::Handle::PRIORITY_DEFAULT, sigc::mem_fun(*this, &Application::on_async_transfer_progress), sigc::mem_fun(*this, &Application::on_sync_transfer_progress));
  }
  catch (Gnome::Vfs::exception& ex)
  {
    std::cout << "Failed to load file information about " << src_uri->get_path() << ": " << ex.what() << std::endl;
    quit();
  }
#else
  std::auto_ptr<Gnome::Vfs::exception> error;

  src_info = src_uri->get_file_info(Gnome::Vfs::FILE_INFO_DEFAULT, error);
  if (! error.get())
  {
    if (src_info->get_type() == Gnome::Vfs::FILE_TYPE_DIRECTORY)
    {
      //Build the destination URI:
      dest_uri = dest_uri->append_string(src_uri->extract_short_path_name());
    }

    src_uri_list.push_back(src_uri);
    dest_uri_list.push_back(dest_uri);

    std::cout << "Transfering file(s)." << std::endl;

    handle_.transfer(src_uri_list, dest_uri_list, Gnome::Vfs::XFER_RECURSIVE, Gnome::Vfs::XFER_ERROR_MODE_QUERY, Gnome::Vfs::XFER_OVERWRITE_MODE_REPLACE, Gnome::Vfs::Async2::Handle::PRIORITY_DEFAULT, sigc::mem_fun(*this, &Application::on_async_transfer_progress), sigc::mem_fun(*this, &Application::on_sync_transfer_progress), error);

    if (error.get())
    {
      print_vfs_error(*error);
      quit();
    }
  }
  else
  {
    print_vfs_error(*error);
    quit();
  }
#endif //GLIBMM_EXCEPTIONS_ENABLED
}

void Application::run()
{
  std::cout << "Starting main loop." << std::endl;
  main_loop_->run();
}

int main (int argc, char** argv)
{ 
  //Get the command-line arguments:
  std::string input_uri_string, output_uri_string;
  Application app;
  
  if(argc == 2)
  {
    input_uri_string = argv[1];

    app.do_open_read(input_uri_string);
    app.run();
  }
  else if(argc == 3)
  {
    input_uri_string = argv[1];
    output_uri_string = argv[2];

    app.do_transfer(input_uri_string, output_uri_string);
    app.run();
  }
  else
  {
    std::cout << "Call this with an input file path to open and read, such as: " << std::endl;
    std::cout << argv[0] << " ~/file " << std::endl;
    std::cout << "Or with two paths as transfer source and destination, eg: " << std::endl;
    std::cout << argv[0] << " http://www.gnome.org ~/" << std::endl;

    return 0;
  }

  return 0;
}
