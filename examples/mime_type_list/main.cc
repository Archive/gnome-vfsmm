/* main.cc
 *
 * Copyright (C) 2003 gnome-vfsmm Development Team
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include <libgnomevfsmm.h>
#include <iostream>

int
print_error (const Gnome::Vfs::exception& ex)
{
  std::cout << "Error occurred: " << ex.what() << std::endl;
            
  return 1;
}

int main (int argc, char** argv)
{
  //Initialize gnome-vfsmm:
  Gnome::Vfs::init();

  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  try
  {
  #endif //GLIBMM_EXCEPTIONS_ENABLED
    //Get the list of registered MIME-types:
    typedef std::list<Glib::ustring> type_listStrings;
    type_listStrings listStrings = Gnome::Vfs::Mime::get_registered_types();

    std::cout << "Registererd MIME-types:" << std::endl;

    //List each registered MIME-type:
    for(type_listStrings::iterator iter = listStrings.begin(); iter != listStrings.end(); ++iter)
    {
      const Glib::ustring mime_type = *iter;
      std::cout << "MIME-type: name=" << mime_type << std::endl;
      std::cout << "  description=" << Gnome::Vfs::Mime::get_description(mime_type) << std::endl;

      //Get the default registered application to handle this MIME type:
      Gnome::Vfs::MimeApplication app = Gnome::Vfs::Mime::get_default_application(mime_type);
      if(!app)
        std::cout << "  No default application registered." << std::endl;
      else
      {
        std::cout << "  Default application name=" << app.get_name() << ", command=" << app.get_command() << std::endl;

        //Get the list of registered applications that can handle this MIME type:
        /* //TODO: These should be by-value, instead of by pointer.
        std::cout << "  All registered applications for this MIME-type";
        typedef std::list<Gnome::Vfs::MimeApplication> type_listApps;
        type_listApps listApps = Gnome::Vfs::Mime::get_all_applications(mime_type);
        for(type_listApps::iterator iter = listApps.begin(); iter != listApps.end(); ++iter)
        {
          Gnome::Vfs::MimeApplication app = *iter;
          if(app)
            std::cout << "    name=" << app.get_name() << ", command=" << app.get_command() << std::endl;
        }
        */
      }
    }
  #ifdef GLIBMM_EXCEPTIONS_ENABLED
  }
  catch(const Gnome::Vfs::exception& ex)
  {
  /* if the operation was not successful, print the error and abort */
    return print_error(ex);
  }
  #endif //GLIBMM_EXCEPTIONS_ENABLED
  
  return 0;
}


