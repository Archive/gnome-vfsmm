/* main.cc
 *
 * Copyright (C) 2003 gnome-vfsmm Development Team
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <libgnomevfsmm.h>

#include <glibmm.h>
#include <iostream>
#include <unistd.h>

Glib::RefPtr<Glib::MainLoop> main_loop;

int print_error (const Gnome::Vfs::exception& ex, const std::string& uri_string)
{
  std::cout << "Error occurred opening locaiton " << uri_string << std:: endl
            << "  " << ex.what() << std::endl;

  return 1; //TODO: Isn't there a nice standard C constant we should be using instead? Murray.
}

void on_monitor_event(const Gnome::Vfs::MonitorHandle& handle, const Glib::ustring& monitor_uri, const Glib::ustring& info_uri, Gnome::Vfs::MonitorEventType type)
{
  static int i = 0; //TODO: What is this for? Murray.

  std::cout << "Got a callback: ";

  switch(type)
  {
    case Gnome::Vfs::MONITOR_EVENT_CHANGED:
      std::cout << "MONITOR_EVENT_CHANGED";
      break;
    case Gnome::Vfs::MONITOR_EVENT_DELETED:
      std::cout << "MONITOR_EVENT_DELETED";
      break;
    case Gnome::Vfs::MONITOR_EVENT_STARTEXECUTING:
      std::cout << "MONITOR_EVENT_STARTEXECUTING";
      break;
    case Gnome::Vfs::MONITOR_EVENT_STOPEXECUTING:
      std::cout << "MONITOR_EVENT_STOPEXECUTING";
      break;
    case Gnome::Vfs::MONITOR_EVENT_CREATED:
      std::cout << "MONITOR_EVENT_CREATED";
      break;
    case Gnome::Vfs::MONITOR_EVENT_METADATA_CHANGED:
      std::cout << "MONITOR_EVENT_METADATA_CHANGED";
      break;
    default:
      std::cout << "Unknown monitor type, exiting...";
      exit(1);
  }

  std::cout << " " << info_uri << std::endl;
  i++;
  
  if(i >= 2)
  {
    main_loop->quit();
  }
}

int main(int argc, char** argv)
{
  main_loop = Glib::MainLoop::create();
  Glib::ustring text_uri = "/tmp/";
  Gnome::Vfs::MonitorHandle handle;

  Gnome::Vfs::init();

#ifdef GLIBMM_EXCEPTIONS_ENABLED
  try
  { 
    handle.add(text_uri, Gnome::Vfs::MONITOR_DIRECTORY, sigc::ptr_fun(&on_monitor_event));

    main_loop->run();
  }
  catch(const Gnome::Vfs::exception& ex)
  {
    return print_error(ex, text_uri);
  }
#else
  std::auto_ptr<Gnome::Vfs::exception> error;

  handle.add(text_uri, Gnome::Vfs::MONITOR_DIRECTORY, sigc::ptr_fun(&on_monitor_event), error);

  main_loop->run();

  if(error.get())
  {
    return print_error(*error.get(), text_uri);
  }
#endif //GLIBMM_EXCEPTIONS_ENABLED

  return 0;
}

