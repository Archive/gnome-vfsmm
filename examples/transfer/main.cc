/* main.cc
 *
 * The program copies file(s) from source to destination, recursively.
 *
 * Copyright (C) 2003, 2006 gnome-vfsmm Development Team
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <iostream>
#include <list>
#include <string>

#include <glibmm.h>
#include <libgnomevfsmm.h>

int print_vfs_error(const Gnome::Vfs::exception& ex)
{
  std::cout << "Exception occurred, reason: " << ex.what() << std::endl;
            
  return 1;
}

gint on_transfer_progress(const Gnome::Vfs::Transfer2::ProgressInfo& info)
{ 
  if (info)
  {
    switch (info.get_status())
    {
      case Gnome::Vfs::XFER_PROGRESS_STATUS_VFSERROR:
        std::cout << "VFS Error: "
                  << gnome_vfs_result_to_string(
                       static_cast<GnomeVFSResult>(info.get_vfs_status()))
                  << std::endl;
        // This is a matter of choice (see ErrorAction enum):
        return Gnome::Vfs::XFER_ERROR_ACTION_ABORT;
        
      case Gnome::Vfs::XFER_PROGRESS_STATUS_OVERWRITE:
        std::cout << "Overwriting " << info.get_target_name()
                  << " with " << info.get_source_name() << std::endl;
        // Here, choose a value from OverwriteAction enum.
        // Note that in this example we always say SKIP, which is the same as 
        // specifying it once in the transfer functions. The purpose of
        // ERROR/OVERWRITE_MODE_QUERY is to be able, in "real" code, to make
        // a real decision each time, based on whatever logic you need.
        return Gnome::Vfs::XFER_OVERWRITE_ACTION_SKIP;
        
      case Gnome::Vfs::XFER_PROGRESS_STATUS_OK:
      {
        std::cout << "Status: OK" << std::endl;
        
        switch (info.get_phase())
        {
          case Gnome::Vfs::XFER_PHASE_INITIAL:
            std::cout << "  Initial phase." << std::endl;
            break;
          case Gnome::Vfs::XFER_CHECKING_DESTINATION:
            std::cout << "  Checking destination." << std::endl;
            break;
          case Gnome::Vfs::XFER_PHASE_COLLECTING:
            std::cout << "  Collecting file list." << std::endl;
            break;
          case Gnome::Vfs::XFER_PHASE_READYTOGO:
            std::cout << "  Ready to go!" << std::endl;
            break;
          case Gnome::Vfs::XFER_PHASE_OPENSOURCE:
            std::cout << "  Opening source." << std::endl;
            break;
          case Gnome::Vfs::XFER_PHASE_OPENTARGET:
            std::cout << "  Opening target." << std::endl;
            break;
          case Gnome::Vfs::XFER_PHASE_COPYING:
            std::cout << "  Copying '" << info.get_source_name()
                      << "' to '" << info.get_target_name()
                      << "' (file " << info.get_file_index()
                      << "/" << info.get_total_files()
                      << ", byte " << info.get_bytes_copied()
                      << "/" << info.get_file_size()
                      << " in file, " << info.get_total_bytes_copied()
                      << "/" << info.get_total_bytes() << std::endl;
            break;
          case Gnome::Vfs::XFER_PHASE_MOVING:
            std::cout << "  Moving '" << info.get_source_name()
                      << "' to '" << info.get_target_name()
                      << "' (file " << info.get_file_index()
                      << "/" << info.get_total_files()
                      << ", byte " << info.get_bytes_copied()
                      << "/" << info.get_file_size()
                      << " in file, " << info.get_total_bytes_copied()
                      << "/" << info.get_total_bytes() << std::endl;
            break;
          case Gnome::Vfs::XFER_PHASE_READSOURCE:
            std::cout << "  Reading source." << std::endl;
            break;
          case Gnome::Vfs::XFER_PHASE_CLOSESOURCE:
            std::cout << "  Closing source." << std::endl;
            break;
          case Gnome::Vfs::XFER_PHASE_CLOSETARGET:
            std::cout << "  Closing target." << std::endl;
            break;
          case Gnome::Vfs::XFER_PHASE_DELETESOURCE:
            std::cout << "  Deleting source." << std::endl;
            break;
          case Gnome::Vfs::XFER_PHASE_FILECOMPLETED:
            std::cout << "  Done with '" << info.get_source_name()
                      << "' -> '" << info.get_target_name()
                      << "', going next." << std::endl;
	    //TODO: What does "going next" mean here? If you mean that one file has been created and another will soon be deleted, then say so. Murray.
            break;
          case Gnome::Vfs::XFER_PHASE_COMPLETED:
            std::cout << "  Completed." << std::endl;
            break;
          default:
            std::cout << "  Unexpected phase " << info.get_phase()
                      << std::endl;
        }
      }
      case Gnome::Vfs::XFER_PROGRESS_STATUS_DUPLICATE:
        break;
    }
  }

  //Returning any non-zero value means that we want the transfer to continue.
  return true;
}

int main (int argc, char** argv)
{ 
  //Get the command-line arguments:
  std::string input_uri_string, output_uri_string;
  
  if (argc > 2)
  {
    input_uri_string = argv[1];
    output_uri_string = argv[2];
  }
  else
  {
    std::cout << "Call this with input and output URIs, such as" << std::endl
              << argv[0] << " http://www.gnome.org file:///home/murrayc/"
              << std::endl
              << "or with normal paths, such as" << std::endl
              << argv[0] << " ~/src_dir ~/target_dir" << std::endl;
    return 0;
  }

  //Initialize gnome-vfsmm:
  Gnome::Vfs::init();
  
  Glib::RefPtr<Gnome::Vfs::Uri> src =
    Gnome::Vfs::Uri::create(
      Gnome::Vfs::Uri::make_from_shell_arg(argv[1]));

  Glib::RefPtr<Gnome::Vfs::Uri> dest =
    Gnome::Vfs::Uri::create(
      Gnome::Vfs::Uri::make_from_shell_arg(argv[2]));

  Glib::RefPtr<Gnome::Vfs::FileInfo> info;
#ifdef GLIBMM_EXCEPTIONS_ENABLED
  try
  {
    info = src->get_file_info();
  }
  catch (Gnome::Vfs::exception& ex)
  {
    std::cout << "Failed to load file information: "
              << ex.what() << std::endl;
    return 1;
  }
#else
  std::auto_ptr<Gnome::Vfs::exception> error;
  info = src->get_file_info(Gnome::Vfs::FILE_INFO_DEFAULT, error);
  if(error.get())
  {
    std::cout << "Failed to load file information: "
              << error->what() << std::endl;
    return 1;
  }
#endif
  
  if (info->get_type() == Gnome::Vfs::FILE_TYPE_DIRECTORY)
  {
    Glib::ustring name(src->extract_short_path_name());
    dest = dest->append_string(name);
  }

#ifdef GLIBMM_EXCEPTIONS_ENABLED
  try
  {
    Gnome::Vfs::Transfer2::transfer(src, dest,
       Gnome::Vfs::XFER_RECURSIVE,
       Gnome::Vfs::XFER_ERROR_MODE_QUERY,
       Gnome::Vfs::XFER_OVERWRITE_MODE_QUERY,
       sigc::ptr_fun(&on_transfer_progress));
    
    /*
    // test remove(ustring path)
    Gnome::Vfs::Transfer2::remove(dest->get_path(),
       Gnome::Vfs::XFER_RECURSIVE,
       Gnome::Vfs::XFER_ERROR_MODE_ABORT,
       sigc::ptr_fun(&on_transfer_progress));
    */
  }
  catch (const Gnome::Vfs::exception& ex)
  {
    // if the operation was not successful, print the error and abort
    return print_vfs_error(ex);
  }
#else
  //std::auto_ptr<Gnome::Vfs::exception> error;

  Gnome::Vfs::Transfer2::transfer(src, dest,
     Gnome::Vfs::XFER_RECURSIVE,
     Gnome::Vfs::XFER_ERROR_MODE_QUERY,
     Gnome::Vfs::XFER_OVERWRITE_MODE_QUERY,
     sigc::ptr_fun(&on_transfer_progress),
     error);
  
  /*
  Gnome::Vfs::Transfer2::remove(dest->get_path(),
     Gnome::Vfs::XFER_RECURSIVE,
     Gnome::Vfs::XFER_ERROR_MODE_ABORT,
     sigc::ptr_fun(&on_transfer_progress),
     error);
  */

  if (error.get())
  {
    // if the operation was not successful, print the error and abort
    return print_vfs_error(*error.get());
  }
#endif //GLIBMM_EXCEPTIONS_ENABLED
  
  return 0;
}
