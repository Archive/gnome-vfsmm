/* main.cc
 *
 * Copyright (C) 2003 gnome-vfsmm Development Team
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include <libgnomevfsmm.h>
#include <iostream>


int print_error (const Gnome::Vfs::exception& ex, const std::string& uri_string)
{
  std::cout << "Error occurred opening location " << uri_string << std::endl
            << "  " << ex.what() << std::endl;
            
  return 1;
}

int main (int argc, char** argv)
{
  //Get the command-line arguments:
  std::string input_uri_string, output_uri_string;
  if(argc > 1 )
  {
    input_uri_string = argv[1];
  }
  else
  {
    std::cout << "Call this with a URLs, such as" << std::endl
              << "./example file:///home/murrayc/" << std::endl;
    return 0;
  }

  //Initialize gnome-vfsmm:
  Gnome::Vfs::init();
 
#ifdef GLIBMM_EXCEPTIONS_ENABLED
  try
  {
    Gnome::Vfs::DirectoryHandle handle;
    handle.open(input_uri_string, Gnome::Vfs::FILE_INFO_DEFAULT | 
                                  Gnome::Vfs::FILE_INFO_GET_MIME_TYPE |
                                  Gnome::Vfs::FILE_INFO_FORCE_SLOW_MIME_TYPE);

    bool file_exists = true;
    while(file_exists) //read_next() returns false when there are no more files to read.
    {
      Glib::RefPtr<Gnome::Vfs::FileInfo> refFileInfo = handle.read_next(file_exists);
      std::cout << refFileInfo->get_name() << ",    MIME-type: " << refFileInfo->get_mime_type() << std::endl;
    }
  }
  catch(const Gnome::Vfs::exception& ex)
  {
  /* if the operation was not successful, print the error and abort */
    return print_error(ex, input_uri_string);
  }
#else
  std::auto_ptr<Gnome::Vfs::exception> error;

  Gnome::Vfs::DirectoryHandle handle;
  printf ("Open()...\n");
  fflush (stdout);
  handle.open(input_uri_string,
              Gnome::Vfs::FILE_INFO_DEFAULT | 
                  Gnome::Vfs::FILE_INFO_GET_MIME_TYPE |
                  Gnome::Vfs::FILE_INFO_FORCE_SLOW_MIME_TYPE,
              error);
  printf ("Handle opened\n");
  fflush (stdout);

  if(error.get())
  {
    printf ("Error set from open()\n");
    fflush (stdout);
    /* if the operation was not successful, print the error and abort */
    return print_error(*error.get(), input_uri_string);
  }
  else
  {
    printf ("Error not set from open()\n");
    fflush (stdout);
  }

  bool file_exists = true;
  while(file_exists) //read_next() returns false when there are no more files to read.
  {
    Glib::RefPtr<Gnome::Vfs::FileInfo> refFileInfo = handle.read_next(file_exists, error);

    if(error.get())
    {
      /* if the operation was not successful, print the error and abort */
      return print_error(*error.get(), input_uri_string);
    }

    std::cout << refFileInfo->get_name() << ",    MIME-type: " << refFileInfo->get_mime_type() << std::endl;
  }
#endif //GLIBMM_EXCEPTIONS_ENABLED
  
 
  return 0;
}


