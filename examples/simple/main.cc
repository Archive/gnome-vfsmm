/* main.cc
 *
 * Copyright (C) 2003 gnome-vfsmm Development Team
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include <libgnomevfsmm.h>
#include <iostream>

#define BYTES_TO_PROCESS 256


int
print_error (const Gnome::Vfs::exception& ex, const std::string& uri_string)
{
  std::cout << "Error occurred opening location " << uri_string << std::endl
            << "  " << ex.what() << std::endl;
            
  return 1;
}

int main (int argc, char** argv)
{
#ifndef GLIBMM_EXCEPTIONS_ENABLED
  std::auto_ptr<Gnome::Vfs::exception> error;
#endif

  // This should read the contents of one file and output them to another file.
  
  // Get the command-line arguments:
  std::string input_uri_string, output_uri_string;
  if(argc > 2 )
  {
    input_uri_string = argv[1];
    output_uri_string = argv[2];
  }
  else
  {
    std::cout << "Call this with input and output URLs, such as" << std::endl
              << "./example http://www.gnome.org file:///home/murrayc/test.txt" << std::endl;
    return 0;
  }

  // Initialize gnome-vfsmm:
  Gnome::Vfs::init();

  // open the input file for read access
  Gnome::Vfs::Handle read_handle;
  
#ifdef GLIBMM_EXCEPTIONS_ENABLED
  try
  {
    read_handle.open(input_uri_string, Gnome::Vfs::OPEN_READ);
  }
  catch(const Gnome::Vfs::exception& ex)
  {
    // If the operation was not successful, print the error and abort
    return print_error(ex, input_uri_string);
  }
#else
  read_handle.open(input_uri_string, Gnome::Vfs::OPEN_READ, error);

  if(error.get())
  {
    // If the operation was not successful, print the error and abort
    return print_error(*error.get(), input_uri_string);
  }
#endif //GLIBMM_EXCEPTIONS_ENABLED
  
  /* we use create instead of open, because open will not create the file if it does
     not already exist. The last argument is the permissions to use if the file is created,
     the second to last tells GnomeVFS that its ok if the file already exists, and just open it */

  Gnome::Vfs::Handle write_handle;

#ifdef GLIBMM_EXCEPTIONS_ENABLED
  try
  {
    //0660 means "this user and his group can read and write this non-executable file".
    //The 0 prefix means that this is octal.
    write_handle.create(output_uri_string, Gnome::Vfs::OPEN_WRITE, false, 0660);
  }
  catch(const Gnome::Vfs::exception& ex)
  {
    // If the operation was not successful, print the error and abort
    return print_error(ex, output_uri_string);
  }
#else
  //0660 means "this user and his group can read and write this non-executable file".
  //The 0 prefix means that this is octal.
  write_handle.create(output_uri_string, Gnome::Vfs::OPEN_WRITE, false, 0660, error);

  if(error.get())
  {
    // If the operation was not successful, print the error and abort
    return print_error(*error.get(), output_uri_string);
  }
#endif //GLIBMM_EXCEPTIONS_ENABLED


  // Read data from the input uri:
  guint buffer[BYTES_TO_PROCESS]; // For each chunk.
  std::string text_whole; //The whole contents.
  // We use std::string instead of Glib::ustring because it might not be UTF8-encoded. 

  Gnome::Vfs::FileSize bytes_read = 0;
#ifdef GLIBMM_EXCEPTIONS_ENABLED
  try
  {
#endif //GLIBMM_EXCEPTIONS_ENABLED
    bool bContinue = true;
    while(bContinue)
    {
#ifdef GLIBMM_EXCEPTIONS_ENABLED
      bytes_read = read_handle.read(buffer, BYTES_TO_PROCESS);
#else
      bytes_read = read_handle.read(buffer, BYTES_TO_PROCESS, error);

      if(error.get())
      {
        // If the operation was not successful, print the error and abort
        return print_error(*error.get(), input_uri_string);
      }
#endif //GLIBMM_EXCEPTIONS_ENABLED
    
      if(bytes_read == 0)
        bContinue = false; //stop because we reached the end.
      else
      {
        // Add the text to the string:
        text_whole += std::string((char*)buffer, bytes_read);
      }
    }
#ifdef GLIBMM_EXCEPTIONS_ENABLED
  }
  catch(const Gnome::Vfs::exception& ex)
  {
    // If the operation was not successful, print the error and abort
    return print_error(ex, input_uri_string);
  }
#endif //GLIBMM_EXCEPTIONS_ENABLED
  
  //Now we have the contents of the file in text_whole.
  //We are assuming that it is a text file.
  //If you want to display or process that text, you would need to know, or guess,
  //the encoding. If it is not UTF8-encoded then you might use Glib::convert() or 
  //Glib::locate_to_utf8() to convert it to a UTF8 string that you can put in a
  //Glib::ustring().

  // Seek to the end of the output uri so we will append rather than overwrite.
  // Therefore, we seek 0 bytes relative to the end of the file
#ifdef GLIBMM_EXCEPTIONS_ENABLED
  try
  {
    write_handle.seek(Gnome::Vfs::SEEK_POS_END, 0);

    // Now write the data we read out to the output uri
    GnomeVFSFileSize bytes_written = write_handle.write(text_whole.data(), text_whole.size());
  }
  catch(const Gnome::Vfs::exception& ex)
  {
    // If the operation was not successful, print the error and abort
    return print_error(ex, output_uri_string);
  }
#else
  write_handle.seek(Gnome::Vfs::SEEK_POS_END, 0, error);

  if(error.get())
  {
    // If the operation was not successful, print the error and abort
    return print_error(*error.get(), output_uri_string);
  }

  // Now write the data we read out to the output uri
  GnomeVFSFileSize bytes_written = write_handle.write(text_whole.data(), text_whole.size(), error);

  if(error.get())
  {
    // If the operation was not successful, print the error and abort
    return print_error(*error.get(), output_uri_string);
  }
#endif //GLIBMM_EXCEPTIONS_ENABLED
  
  return 0;
}


